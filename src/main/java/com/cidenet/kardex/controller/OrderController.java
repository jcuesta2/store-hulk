/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.controller;

import com.cidenet.kardex.model.OrderModel;
import com.cidenet.kardex.model.PersonageModel;
import com.cidenet.kardex.model.PriceOfProductModel;
import com.cidenet.kardex.model.ProductTypeModel;
import com.cidenet.kardex.model.SizeModel;
import com.cidenet.kardex.model.SupplierModel;
import com.cidenet.kardex.modelDao.OrderDao;
import com.cidenet.kardex.modelDao.PersonageDao;
import com.cidenet.kardex.modelDao.PriceOfProductDao;
import com.cidenet.kardex.modelDao.ProductTypeDao;
import com.cidenet.kardex.modelDao.SizeDao;
import com.cidenet.kardex.modelDao.StorageDao;
import com.cidenet.kardex.modelDao.SupplierDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

/**
 *
 * @author jcuesta
 */
@WebServlet(name = "OrderController", urlPatterns = {"/OrderController"})
public class OrderController extends HttpServlet {

    List<OrderModel> orderList, EditModal = new ArrayList<>();
    List<ProductTypeModel> proAssetList = new ArrayList<>();
    List<SupplierModel> supplierAssetList = new ArrayList<>();
    List<SizeModel> sizeAssetList = new ArrayList<>();
    List<PersonageModel> personageAssetList = new ArrayList<>();
    List<PriceOfProductModel> priceList = new ArrayList<>();

    OrderDao order = new OrderDao();
    ProductTypeDao product = new ProductTypeDao();
    SupplierDao supplier = new SupplierDao();
    SizeDao size = new SizeDao();
    PersonageDao personage = new PersonageDao();
    PriceOfProductDao price = new PriceOfProductDao();
    StorageDao storage = new StorageDao();

    OrderModel orderModel = new OrderModel();
    PriceOfProductModel priceModel = new PriceOfProductModel();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet OrderController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet OrderController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String codeStatus = "";
        String message = "";
        String openModal = "";
        String dateLimit = "";
        String dateStart = "";
        int orderId;
        String action = request.getParameter("action");

        switch (action) {
            case "OrderList": {
                
            }
            break;

            case "JoinToSelect": {
                openModal = "Yes";
                proAssetList = product.AssetList();
                supplierAssetList = supplier.AssetList();
                sizeAssetList = size.AssetList();
                personageAssetList = personage.AssetList();

                request.setAttribute("openModal", openModal);
                request.setAttribute("proAssetList", proAssetList);
                request.setAttribute("supplierAssetList", supplierAssetList);
                request.setAttribute("sizeAssetList", sizeAssetList);
                request.setAttribute("personageAssetList", personageAssetList);

            }
            break;
            case "OrderFilter": {

                try {
                    dateStart = request.getParameter("txtDateStart");
                    dateLimit = request.getParameter("txtDateLimit");
                    int compare = dateLimit.compareTo(dateStart);

                    if (compare >= 0) {
                        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

                        Date start = format1.parse(dateStart);
                        dateStart = formatter.format(start);
                        if (dateLimit.equals("")) {
                            Date date = new Date();
                            dateLimit = new SimpleDateFormat("dd/MM/yyyy").format(date);
                        } else {
                            Date limit = format1.parse(dateLimit);
                            dateLimit = formatter.format(limit);
                        }

                       
                    } else {
                        codeStatus = "Error";
                        message = "La fecha  inicial no puede ser mayor a la fecha final de la busqueda";
                    }

                } catch (ParseException ex) {
                    Logger.getLogger(OrderController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            break;
            case "Receive": {
                orderId = Integer.parseInt(request.getParameter("orderId"));
                orderList = order.OrderReceive(orderId); //validar si es mejor filtrar la lista
                if (storage.ValidateStorageExistence(orderList)) {
                    order.ChangeStatus(orderId, "Entregado");
                    codeStatus = "Success";
                    message = "Producto recibido y almacenado correctamente";
                } else {
                    codeStatus = "Error";
                    message = "Error recibiendo o almacenando entrega";
                }

            }
            break;
            
            case "Cancel": {
                int resp = JOptionPane.showConfirmDialog(null, "¿Está seguro que desea cancelar el pedido? "
                        + "El proveedor no recibira este pedido", "Alerta!", JOptionPane.YES_NO_OPTION);
                if (resp == 0) {
                    orderId = Integer.parseInt(request.getParameter("orderId"));
                    if (order.Cancel(orderId)) {
                        codeStatus = "Success";
                        message = "Pedido Cancelado, el proveedor no recibira ese pedido";
                    } else {
                        codeStatus = "Error";
                        message = "Error Cancelado pedido";
                    }
                } else {
                    codeStatus = "Información";
                    message = "No se cancelo el pedido";
                }

            }
            break;
        }

        if (action.equals("OrderFilter")) {
             orderList = order.OrderFilter(dateStart, dateLimit);
        }else{
            orderList = order.List();
        }
        request.setAttribute("codeStatus", codeStatus);
        request.setAttribute("message", message);
        request.setAttribute("orderList", orderList);
        request.getRequestDispatcher("com.cidenet.kardex.view/order.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String codeStatus = "";
        String message = "";
        String action = request.getParameter("action");
        switch (action) {
            case "OrderAdd": {

                priceModel.setSupplierId(request.getParameter("txtsupplier"));
                priceModel.setProductTypeId(request.getParameter("txtproduct"));

                priceList = price.ValidatePriceExistence(priceModel);
                if (priceList != null) {
                    Date date = new Date();
                    String aplicationDate = new SimpleDateFormat("dd/MM/yyyy").format(date);

                    orderModel.setStore("1");
                    orderModel.setPriceProductId(priceList.get(0).getPriceOfProductId());
                    orderModel.setQuantity(Integer.parseInt(request.getParameter("txtquantity")));
                    orderModel.setApplicationDate(aplicationDate);
                    orderModel.setUnityValue(priceList.get(0).getSupplierPrice());
                    orderModel.setTotalValue(orderModel.getUnityValue() * orderModel.getQuantity());
                    orderModel.setStatus("En espera");
                    orderModel.setPersonage(request.getParameter("txtpersonage"));
                    orderModel.setSize(request.getParameter("txtsize"));

                    if (order.ValidateOrderExistence(orderModel)) {
                        codeStatus = "Success";
                        message = "Pedido enviado correctamente";
                    }

                } else {

                    codeStatus = "Alerta";
                    message = "Debes acordar el valor del producto seleccionado con el proveedor "
                            + "¿Deseas ingresar el valor acordado del producto?";
                }

            }
            break;

        }
        orderList = order.List();
        request.setAttribute("codeStatus", codeStatus);
        request.setAttribute("message", message);
        request.setAttribute("orderList", orderList);
        request.getRequestDispatcher("com.cidenet.kardex.view/order.jsp").forward(request, response);

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
