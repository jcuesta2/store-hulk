/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.modelDao;

import com.cidenet.kardex.configuration.Conexion;
import com.cidenet.kardex.interfaces.General;
import com.cidenet.kardex.interfaces.Supplier;
import com.cidenet.kardex.model.SupplierModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author jcuesta
 */
public class SupplierDao implements General, Supplier {

    String SQL, banUpdateDelete = "";
    private boolean statusMessage = false;
    Conexion conn = Conexion.getInstance();

    @Override
    public List List() {

        List<SupplierModel> supplierList = new ArrayList<>();

        try {
            SQL = "SELECT id_editorial, initcap(descripcion), initcap(estado) "
                    + "FROM Editoriales "
                    + "WHERE  estado != ? "
                    + "ORDER BY descripcion ASC";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, "eliminado");
            conn.resultado = conn.sentencia.executeQuery();

            while (conn.resultado.next()) {
                SupplierModel supplier = new SupplierModel();
                supplier.setEditorialId(conn.resultado.getInt(1));
                supplier.setDescription(conn.resultado.getString(2));
                supplier.setStatus(conn.resultado.getString(3));

                supplierList.add(supplier);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar existencia de proveedor \n" + e);
        }
        return supplierList;

    }

    @Override
    public List Search(String description) {
        List<SupplierModel> supplierList = new ArrayList<>();

        try {
            SQL = "SELECT id_editorial, initcap(descripcion), initcap(estado) "
                    + " FROM Editoriales WHERE descripcion LIKE ? "
                    + "and estado != ? "
                    + "ORDER BY descripcion ASC";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            String filter = "%".concat(description.toLowerCase()).concat("%");
            conn.sentencia.setString(1, filter);
            conn.sentencia.setString(2, "eliminado");
            conn.resultado = conn.sentencia.executeQuery();

            while (conn.resultado.next()) {
                SupplierModel supplier = new SupplierModel();
                supplier.setEditorialId(conn.resultado.getInt(1));
                supplier.setDescription(conn.resultado.getString(2));
                supplier.setStatus(conn.resultado.getString(3));

                supplierList.add(supplier);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar existencia de proveedor \n" + e);
        }
        return supplierList;

    }

    @Override
    public boolean Add(SupplierModel supplier) {

        if (!SupplierValidate(supplier)) {
            try {

                SQL = "INSERT INTO Editoriales (descripcion,estado) values (?,?)";

                conn.sentencia = conn.conexion.prepareStatement(SQL);
                conn.sentencia.setString(1, supplier.getDescription().toLowerCase());
                conn.sentencia.setString(2, supplier.getStatus().toLowerCase());

                conn.sentencia.executeUpdate();
                statusMessage = true;

            } catch (SQLException e) {

                JOptionPane.showInputDialog("Error al consultar existencia de proveedor \n" + e);
            }
        } else {
            statusMessage = banUpdateDelete.equals("UpdateDelete");
        }

        return statusMessage;

    }

    @Override
    public List AssetList() {
        List<SupplierModel> supplierList = new ArrayList<>();

        try {
            SQL = "SELECT * FROM Editoriales WHERE estado = ?";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, "activo");
            conn.resultado = conn.sentencia.executeQuery();

            while (conn.resultado.next()) {
                SupplierModel supplier = new SupplierModel();
                supplier.setEditorialId(conn.resultado.getInt(1));
                supplier.setDescription(conn.resultado.getString(2));

                supplierList.add(supplier);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar existencia de proveedor \n" + e);
        }
        return supplierList;

    }

    @Override
    public boolean ChangeStatus(SupplierModel supplier) {

        SQL = "UPDATE Editoriales SET estado = ? WHERE id_editorial = ?";

        try {
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, supplier.getStatus().toLowerCase());
            conn.sentencia.setInt(2, supplier.getEditorialId());

            conn.sentencia.executeUpdate();
            statusMessage = true;
        } catch (SQLException error) {
            JOptionPane.showInputDialog("Error al cambiar el estado del proveedor: \n" + error);

        }
        return statusMessage;

    }

    @Override
    public boolean Edit(SupplierModel supplier) {
        int id = supplier.getEditorialId();
        SupplierValidate(supplier);
        if (id == supplier.getEditorialId()) {
            Update(supplier);
            statusMessage = true;
        }
        return statusMessage;

    }

    public void Update(SupplierModel supplier) {
        try {
            SQL = "UPDATE Editoriales "
                    + "SET  descripcion = ?, estado = ? "
                    + " WHERE id_editorial = ?";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, supplier.getDescription().toLowerCase());
            conn.sentencia.setString(2, supplier.getStatus().toLowerCase());
            conn.sentencia.setInt(3, supplier.getEditorialId());
            conn.sentencia.executeUpdate();

        } catch (SQLException error) {
            JOptionPane.showInputDialog("Error al actualizar los datos : \n" + error);

        }
    }

    public boolean SupplierValidate(SupplierModel supplier) {
        try {
            SQL = "SELECT id_editorial, estado FROM Editoriales WHERE descripcion = ?";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, supplier.getDescription().toLowerCase());
            conn.resultado = conn.sentencia.executeQuery();

            if (conn.resultado.next()) {
                supplier.setEditorialId(conn.resultado.getInt(1));
                String status = conn.resultado.getString(2);
                if (status.equals("eliminado")) {
                    supplier.setStatus("activo");
                    Update(supplier);
                    banUpdateDelete = "UpdateDelete";
                }
                statusMessage = true;
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar existencia de la talla : \n" + e);
        }
        return statusMessage;
    }

}
