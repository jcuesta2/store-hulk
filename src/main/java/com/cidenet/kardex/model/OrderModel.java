/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.model;

/**
 *
 * @author jcuesta
 */
public class OrderModel {

    private int orderId;
    private int storageId;
    private String store;
    private String personage;
    private String productType;
    private int quantity;
    private String size;
    private String applicationDate;
    private String supplier;
    private int totalValue;
    private int unityValue;
    private String status;
    private int priceProductId;

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getStorageId() {
        return storageId;
    }

    public void setStorageId(int storageId) {
        this.storageId = storageId;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public String getPersonage() {
        return personage;
    }

    public void setPersonage(String personage) {
        this.personage = personage;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(String applicationDate) {
        this.applicationDate = applicationDate;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public int getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(int totalValue) {
        this.totalValue = totalValue;
    }

    public int getUnityValue() {
        return unityValue;
    }

    public void setUnityValue(int unityValue) {
        this.unityValue = unityValue;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getPriceProductId() {
        return priceProductId;
    }

    public void setPriceProductId(int priceProductId) {
        this.priceProductId = priceProductId;
    }

    public OrderModel(int orderId, int storageId, String store, String personage, String productType, int quantity, String size, String applicationDate, String supplier, int totalValue, int unityValue, String status, int priceProductId) {
        this.orderId = orderId;
        this.storageId = storageId;
        this.store = store;
        this.personage = personage;
        this.productType = productType;
        this.quantity = quantity;
        this.size = size;
        this.applicationDate = applicationDate;
        this.supplier = supplier;
        this.totalValue = totalValue;
        this.unityValue = unityValue;
        this.status = status;
        this.priceProductId = priceProductId;
    }

    @Override
    public String toString() {
        return "OrderModel{" + "orderId=" + orderId + ", storageId=" + storageId + ", store=" + store + ", personage=" + personage + ", productType=" + productType + ", quantity=" + quantity + ", size=" + size + ", applicationDate=" + applicationDate + ", supplier=" + supplier + ", totalValue=" + totalValue + ", unityValue=" + unityValue + ", status=" + status + ", priceProductId=" + priceProductId + '}';
    }

    public OrderModel() {
    }
    
    
    
  

}