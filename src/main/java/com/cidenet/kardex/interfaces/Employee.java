/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.interfaces;

import com.cidenet.kardex.model.EmployeeModel;
import java.util.List;

/**
 *
 * @author jcuesta
 */
public interface Employee {

    public boolean Add(EmployeeModel employee);

    public boolean Edit(EmployeeModel employee);
    
    public boolean ChangeStatus(int id, String status);
    
    public List AssetList();
  
}
