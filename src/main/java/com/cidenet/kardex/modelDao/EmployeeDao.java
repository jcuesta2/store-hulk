/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.modelDao;

import com.cidenet.kardex.configuration.Conexion;
import com.cidenet.kardex.interfaces.Employee;
import com.cidenet.kardex.interfaces.General;
import com.cidenet.kardex.model.EmployeeModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author jcuesta
 */
public class EmployeeDao implements Employee, General {

    String SQL = "";
    private boolean validExistence = false;
    private boolean statusMessage = false;
    Conexion conn = Conexion.getInstance();

    @Override
    public boolean Add(EmployeeModel employee) {
        if (!EmployeeValidate(employee)) {
            try {

                SQL = "INSERT INTO Empleados (tipo_documento, numero_documento, nombre, apellido, contacto, estado)"
                        + " values (?,?,?,?,?,?)";

                conn.sentencia = conn.conexion.prepareStatement(SQL);
                conn.sentencia.setInt(1, Integer.parseInt(employee.getDocumentType()));
                conn.sentencia.setString(2, employee.getDocumentNumber());
                conn.sentencia.setString(3, employee.getName().toLowerCase());
                conn.sentencia.setString(4, employee.getLastname().toLowerCase());
                conn.sentencia.setString(5, employee.getContact().toLowerCase());
                conn.sentencia.setString(6, employee.getStatus().toLowerCase());

                conn.sentencia.executeUpdate();
                statusMessage = true;

            } catch (SQLException e) {
                JOptionPane.showInputDialog("Error al registrar   empleado \n" + e);
            }
        } else {
            statusMessage = false;
        }
        return statusMessage;
    }

    @Override
    public boolean Edit(EmployeeModel employee) {

        if (!EmployeeValidate(employee)) {
            Update(employee);
        }else{
            statusMessage = false;
        }
        return statusMessage;

    }

    public boolean EmployeeValidate(EmployeeModel employee) {
        try {
            SQL = "SELECT id_empleado, estado FROM Empleados WHERE numero_documento = ?";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, employee.getDocumentNumber());
            conn.resultado = conn.sentencia.executeQuery();
            if (conn.resultado.next()) {
                String status = conn.resultado.getString(2);
                if (status.equals("eliminado")) {
                    employee.setEmployeeId(conn.resultado.getInt(1));
                    employee.setStatus("activo");
                    Update(employee);
                    validExistence = true;
                } else {
                    validExistence = employee.getEmployeeId() != conn.resultado.getInt(1);
                }

            } else {
                validExistence = false;
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar existencia de empleado : \n" + e);
        }

        return validExistence;

    }

    @Override
    public List List() {

        List<EmployeeModel> employeeList = new ArrayList<>();

        try {
            SQL = "SELECT e.id_empleado, e.numero_documento, initcap(e.nombre), initcap(e.apellido), initcap(e.contacto), initcap(e.estado), initcap(td.descripcion) "
                    + "FROM Empleados e join TiposDocumentos td "
                    + "on e.tipo_documento = td.id_tipo_documento "
                    + "WHERE  e.estado != ? "
                    + "ORDER BY e.numero_documento ASC";

            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, "eliminado");
            conn.resultado = conn.sentencia.executeQuery();
            while (conn.resultado.next()) {
                EmployeeModel employee = new EmployeeModel();
                employee.setEmployeeId(conn.resultado.getInt(1));
                employee.setDocumentNumber(conn.resultado.getString(2));
                employee.setName(conn.resultado.getString(3));
                employee.setLastname(conn.resultado.getString(4));
                employee.setContact(conn.resultado.getString(5));
                employee.setStatus(conn.resultado.getString(6));
                employee.setDocumentType(conn.resultado.getString(7));

                employeeList.add(employee);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar existencia de empleado \n" + e);
        }
        return employeeList;

    }

    @Override
    public List Search(String description) {
        List<EmployeeModel> employeeList = new ArrayList<>();

        try {
            SQL = "SELECT e.id_empleado, e.numero_documento, initcap(e.nombre), initcap(e.apellido), initcap(e.contacto), initcap(e.estado), initcap(td.descripcion) "
                    + " FROM Empleados e  join TiposDocumentos td on e.tipo_documento = td.id_tipo_documento "
                    + "WHERE e.numero_documento LIKE ? and  e.estado != ? "
                    + "ORDER BY e.numero_documento ASC";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            String filter = "%".concat(description.toLowerCase()).concat("%");
            conn.sentencia.setString(1, filter);
            conn.sentencia.setString(2, "eliminado");
            conn.resultado = conn.sentencia.executeQuery();

            while (conn.resultado.next()) {
                EmployeeModel employee = new EmployeeModel();
                employee.setEmployeeId(conn.resultado.getInt(1));
                employee.setDocumentNumber(conn.resultado.getString(2));
                employee.setName(conn.resultado.getString(3));
                employee.setLastname(conn.resultado.getString(4));
                employee.setContact(conn.resultado.getString(5));
                employee.setStatus(conn.resultado.getString(6));
                employee.setDocumentType(conn.resultado.getString(7));

                employeeList.add(employee);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar existencia de empleado \n" + e);
        }
        return employeeList;

    }

    @Override
    public List AssetList() {
        List<EmployeeModel> employeeList = new ArrayList<>();

        try {
            SQL = "SELECT e.id_empleado, e.numero_documento, initcap(e.nombre), initcap(e.apellido), initcap(e.contacto), initcap(e.estado), initcap(td.descripcion) "
                    + " FROM Empleados e  join TiposDocumentos td on e.tipo_documento = td.id_tipo_documento "
                    + "WHERE   e.estado = ? "
                    + "ORDER BY e.numero_documento ASC";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, "activo");
            conn.resultado = conn.sentencia.executeQuery();

            while (conn.resultado.next()) {
                EmployeeModel employee = new EmployeeModel();
                employee.setEmployeeId(conn.resultado.getInt(1));
                employee.setDocumentNumber(conn.resultado.getString(2));
                employee.setName(conn.resultado.getString(3));
                employee.setLastname(conn.resultado.getString(4));
                employee.setContact(conn.resultado.getString(5));
                employee.setStatus(conn.resultado.getString(6));
                employee.setDocumentType(conn.resultado.getString(7));

                employeeList.add(employee);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar existencia de empleado \n" + e);
        }
        return employeeList;
    }

    @Override
    public boolean ChangeStatus(int id, String status) {

        SQL = "UPDATE Empleados SET estado = ?  WHERE id_empleado = ?";

        try {
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, status);
            conn.sentencia.setInt(2, id);
            conn.sentencia.executeUpdate();

            statusMessage = true;
        } catch (SQLException error) {
            JOptionPane.showInputDialog("Error al cambiar el estado del empleado: \n" + error);

        }
        return statusMessage;

    }

    public void Update(EmployeeModel employee) {

        try {
            SQL = "UPDATE Empleados "
                    + "SET  tipo_documento = ?, numero_documento = ?, nombre = ?, apellido = ?, contacto = ?, estado = ?"
                    + " WHERE id_empleado = ?";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setInt(1, Integer.parseInt(employee.getDocumentType()));
            conn.sentencia.setString(2, employee.getDocumentNumber());
            conn.sentencia.setString(3, employee.getName().toLowerCase());
            conn.sentencia.setString(4, employee.getLastname().toLowerCase());
            conn.sentencia.setString(5, employee.getContact().toLowerCase());
            conn.sentencia.setString(6, employee.getStatus().toLowerCase());
            conn.sentencia.setInt(7, employee.getEmployeeId());
            conn.sentencia.executeUpdate();
            statusMessage = true;
        } catch (SQLException error) {
            JOptionPane.showInputDialog("Error al actualizar los datos : \n" + error);

        }

    }

}
