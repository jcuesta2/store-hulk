/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.modelDao;

import com.cidenet.kardex.configuration.Conexion;
import com.cidenet.kardex.interfaces.General;

import com.cidenet.kardex.interfaces.Storage;
import com.cidenet.kardex.model.OrderModel;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author jcuesta
 */
public class StorageDao implements Storage, General {

    String SQL = "";
    private boolean statusMessage = false;
    Conexion conn = Conexion.getInstance();

    @Override
    public boolean ValidateStorageExistence(List<OrderModel> order) {

        try {
            SQL = "SELECT  id_producto_bodega,stock "
                    + " FROM ProductosBodega "
                    + " WHERE (superHeroe = ?) "
                    + " and (tipo_producto = ?) "
                    + " and (talla = ?)";

            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setInt(1, Integer.parseInt(order.get(0).getPersonage()));
            conn.sentencia.setInt(2, Integer.parseInt(order.get(0).getProductType()));
            conn.sentencia.setInt(3, Integer.parseInt(order.get(0).getSize()));

            conn.resultado = conn.sentencia.executeQuery();

            if (conn.resultado.next()) {
                int storageId = conn.resultado.getInt(1);
                int updateQuantity = order.get(0).getQuantity() + conn.resultado.getInt(2);
                int updatePrice = order.get(0).getUnityValue();

                Update(storageId, updateQuantity, updatePrice);
            } else {
                Add(order);
            }
        } catch (NumberFormatException | SQLException e) {
            JOptionPane.showInputDialog("Error al consultar producto en bodega \n" + e);
        }

        return statusMessage;

    }

    public boolean Update(int storageId, int updateQuantity, int updatePrice) {
        try {
            SQL = "UPDATE ProductosBodega SET  stock = ?, precio_productos = ? "
                    + "WHERE id_producto_bodega = ?";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setInt(1, updateQuantity);
            conn.sentencia.setInt(2, updatePrice);
            conn.sentencia.setInt(3, storageId);
            conn.sentencia.executeUpdate();
            statusMessage = true;

        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al actualizar Producto en bodega \n" + e);
        }
        return statusMessage;

    }

    @Override
    public boolean Add(List<OrderModel> order) {
        try {
            SQL = "INSERT INTO ProductosBodega (superHeroe, tipo_producto, talla, stock, precio_productos)"
                    + " values (?, ?, ? ,?, ?)";

            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setInt(1, Integer.parseInt(order.get(0).getPersonage()));
            conn.sentencia.setInt(2, Integer.parseInt(order.get(0).getProductType()));
            conn.sentencia.setInt(3, Integer.parseInt(order.get(0).getSize()));
            conn.sentencia.setInt(4, order.get(0).getQuantity());
            conn.sentencia.setInt(5, order.get(0).getUnityValue());
            conn.sentencia.executeUpdate();
            statusMessage = true;
        } catch (NumberFormatException | SQLException e) {
            JOptionPane.showInputDialog("Error al actualizar Producto en bodega \n" + e);
        }
        return statusMessage;

    }

    @Override
    public List List() {
        List<OrderModel> storageList = new ArrayList<>();

        try {
            SQL = "SELECT  b.id_producto_bodega, tp.descripcion, sh.nombre,  talla.descripcion, "
                    + "b.stock, b.precio_productos "
                    + "FROM ProductosBodega b "
                    + "join TiposProductos tp on b.tipo_producto = tp.id_tipo_producto "
                    + "join Tallas talla on b.talla = talla.id_talla "
                    + "join SuperHeroes sh on b.superHeroe = sh.id_super_heroe"
                    + " ORDER BY tp.descripcion ASC";

            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.resultado = conn.sentencia.executeQuery();

            while (conn.resultado.next()) {
                OrderModel storage = new OrderModel();

                storage.setStorageId(conn.resultado.getInt(1));
                storage.setProductType(conn.resultado.getString(2));
                storage.setPersonage(conn.resultado.getString(3));
                storage.setSize(conn.resultado.getString(4));
                storage.setQuantity(conn.resultado.getInt(5));
                storage.setUnityValue(conn.resultado.getInt(6));

                storageList.add(storage);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar existencia de pedido \n" + e);
        }
        return storageList;

    }

    @Override
    public List Search(String description) {
        List<OrderModel> storageList = new ArrayList<>();

        try {
            SQL = "SELECT  b.id_producto_bodega, tp.descripcion, sh.nombre,  talla.descripcion, "
                    + "b.stock, b.precio_productos "
                    + "FROM ProductosBodega b "
                    + "join TiposProductos tp on b.tipo_producto = tp.id_tipo_producto "
                    + "join Tallas talla on b.talla = talla.id_talla "
                    + "join SuperHeroes sh on b.superHeroe = sh.id_super_heroe"
                    + " WHERE tp.descripcion LIKE ? "
                    + " ORDER BY tp.descripcion ASC";

            conn.sentencia = conn.conexion.prepareStatement(SQL);
            String filter = "%".concat(description.toLowerCase()).concat("%");
            conn.sentencia.setString(1, filter);
            conn.resultado = conn.sentencia.executeQuery();

            while (conn.resultado.next()) {
                OrderModel storage = new OrderModel();

                storage.setStorageId(conn.resultado.getInt(1));
                storage.setProductType(conn.resultado.getString(2));
                storage.setPersonage(conn.resultado.getString(3));
                storage.setSize(conn.resultado.getString(4));
                storage.setQuantity(conn.resultado.getInt(5));
                storage.setUnityValue(conn.resultado.getInt(6));

                storageList.add(storage);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar producto en bodega \n" + e);
        }
        return storageList;

    }

    @Override
    public List UpdateStorageStock(OrderModel store) {
        List<OrderModel> storageList = new ArrayList<>();
        try {
            SQL = "SELECT  id_producto_bodega, precio_productos, stock "
                    + " FROM ProductosBodega "
                    + " WHERE (superHeroe = ?) "
                    + " and (tipo_producto = ?) "
                    + " and (talla = ?)";

            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setInt(1, Integer.parseInt(store.getPersonage()));
            conn.sentencia.setInt(2, Integer.parseInt(store.getProductType()));
            conn.sentencia.setInt(3, Integer.parseInt(store.getSize()));

            conn.resultado = conn.sentencia.executeQuery();

            if (conn.resultado.next() && conn.resultado.getInt(3) >= store.getQuantity()) {
                store.setStorageId(conn.resultado.getInt(1));
                store.setUnityValue(conn.resultado.getInt(2));
                int storageQuantity = conn.resultado.getInt(3);
                
                int stock = (storageQuantity - store.getQuantity());
                if (UpdateStock(store.getStorageId(), stock)) {
                    storageList.add(store);

                }
            } else {
                storageList = null;
            }
        } catch (NumberFormatException | SQLException e) {
            JOptionPane.showInputDialog("Error al consultar producto en bodega \n" + e);
        }

        return storageList;
    }

    public boolean UpdateStock(int id, int stock) {
        try {
            SQL = "UPDATE ProductosBodega SET  stock = ? "
                    + "WHERE id_producto_bodega = ?";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setInt(1, stock);
            conn.sentencia.setInt(2, id);
            conn.sentencia.executeUpdate();
            statusMessage = true;
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al actualizar stock en bodega \n" + e);
        }
        return statusMessage;
    }
}
