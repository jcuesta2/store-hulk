package com.cidenet.kardex.modelDao;

import com.cidenet.kardex.configuration.Conexion;
import com.cidenet.kardex.interfaces.General;
import com.cidenet.kardex.interfaces.Order;
import com.cidenet.kardex.model.OrderModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author jcuesta
 */
public class OrderDao implements Order, General {

    String SQL = "";
    private boolean statusMessage = false;
    Conexion conn = Conexion.getInstance();

    @Override
    public List List() {
        List<OrderModel> orderList = new ArrayList<>();

        try {
            SQL = "SELECT  p.id_pedidos, initcap(prov.descripcion), initcap(tp.descripcion), "
                    + "initcap(sh.nombre),  initcap(talla.descripcion), "
                    + "p.cantidad, p.fecha_solicitud, pp.id_precio_productos, "
                    + "p.valor_unidad, p.valor, initcap(p.estado) "
                    + "FROM PreciosProductos pp "
                    + "join TiposProductos tp on pp.tipo_producto = tp.id_tipo_producto "
                    + "join Editoriales prov on pp.editorial = prov.id_editorial "
                    + "join Pedidos p on p.id_precios_productos = pp.id_precio_productos  "
                    + "join SuperHeroes sh on p.id_super_heroe = sh.id_super_heroe "
                    + "join Tallas talla on p.talla_tamano = talla.id_talla "
                    + "ORDER BY p.fecha_solicitud DESC";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.resultado = conn.sentencia.executeQuery();

            while (conn.resultado.next()) {
                OrderModel order = new OrderModel();
                order.setOrderId(conn.resultado.getInt(1));
                order.setSupplier(conn.resultado.getString(2));
                order.setProductType(conn.resultado.getString(3));
                order.setPersonage(conn.resultado.getString(4));
                order.setSize(conn.resultado.getString(5));
                order.setQuantity(conn.resultado.getInt(6));
                order.setApplicationDate(conn.resultado.getString(7));
                order.setPriceProductId(conn.resultado.getInt(8));
                order.setUnityValue(conn.resultado.getInt(9));
                order.setTotalValue(conn.resultado.getInt(10));
                order.setStatus(conn.resultado.getString(11));

                orderList.add(order);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar existencia de pedido \n" + e);
        }
        return orderList;

    }

    @Override
    public java.util.List Search(String description) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean ChangeStatus(int orderId, String value) {

        SQL = "UPDATE Pedidos SET estado=? WHERE id_pedidos=?";

        try {
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, value.toLowerCase());
            conn.sentencia.setInt(2, orderId);

            conn.sentencia.executeUpdate();
            statusMessage = true;
        } catch (SQLException error) {
            JOptionPane.showInputDialog("Error al cambiar el estado del pedido: \n" + error);

        }
        return statusMessage;

    }

    @Override
    public boolean ValidateOrderExistence(OrderModel order) {
        try {
            SQL = "SELECT  id_pedidos, cantidad, valor "
                    + " FROM Pedidos "
                    + " WHERE id_precios_productos = ? "
                    + "and fecha_solicitud = ? "
                    + "and id_super_heroe = ? "
                    + "and talla_tamano = ? "
                    + "and estado != ?";

            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setInt(1, order.getPriceProductId());
            conn.sentencia.setString(2, order.getApplicationDate());
            conn.sentencia.setInt(3, Integer.parseInt(order.getPersonage()));
            conn.sentencia.setInt(4, Integer.parseInt(order.getSize()));
            conn.sentencia.setString(5, "entregado");
            conn.resultado = conn.sentencia.executeQuery();

            if (conn.resultado.next()) {

                int orderId = conn.resultado.getInt(1);
                int updateQuantity = order.getQuantity() + conn.resultado.getInt(2);
                int updateValue = order.getTotalValue()+ conn.resultado.getInt(3);

                Update(orderId, updateQuantity, updateValue);

            } else {
                Add(order);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar pedido \n" + e);
        }
        return statusMessage;
    }

    public boolean Update(int orderId, int updateQuantity, int updateValue) {
        try {
            SQL = "UPDATE Pedidos SET  cantidad = ?, valor = ? "
                    + "WHERE id_pedidos = ?";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setInt(1, updateQuantity);
            conn.sentencia.setInt(2, updateValue);
            conn.sentencia.setInt(3, orderId);
            conn.sentencia.executeUpdate();
            statusMessage = true;

        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al actualizar pedido \n" + e);
        }
        return statusMessage;

    }

    @Override
    public boolean Add(OrderModel order) {
        try {
            SQL = "INSERT INTO Pedidos (id_tienda, id_precios_productos, cantidad, fecha_solicitud, valor_unidad, valor, estado, id_super_heroe, talla_tamano) \n"
                    + "values (?, ?, ?, ?, ?, ? ,?, ?, ?)";

            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setInt(1, Integer.parseInt(order.getStore()));
            conn.sentencia.setInt(2, order.getPriceProductId());
            conn.sentencia.setInt(3, order.getQuantity());
            conn.sentencia.setString(4, order.getApplicationDate());
            conn.sentencia.setInt(5, order.getUnityValue());
            conn.sentencia.setInt(6, order.getTotalValue());
            conn.sentencia.setString(7, order.getStatus());
            conn.sentencia.setInt(8, Integer.parseInt(order.getPersonage()));
            conn.sentencia.setInt(9, Integer.parseInt(order.getSize()));
            conn.sentencia.executeUpdate();
            statusMessage = true;
        } catch (SQLException ex) {
            Logger.getLogger(OrderDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return statusMessage;
    }

    @Override
    public List OrderReceive(int OrderId) {
        List<OrderModel> orderList = new ArrayList<>();
        try {
            SQL = "SELECT p.id_super_heroe, p.cantidad, p.talla_tamano, "
                    + " pp.tipo_producto,  pp.unidad_precio_tienda "
                    + " FROM Pedidos p "
                    + " JOIN PreciosProductos pp on p.id_precios_productos = pp.id_precio_productos "
                    + " WHERE id_pedidos = ?";

            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setInt(1, OrderId);
            conn.resultado = conn.sentencia.executeQuery();
            if (conn.resultado.next()) {
                OrderModel order = new OrderModel();
                order.setPersonage(String.valueOf(conn.resultado.getInt(1)));
                order.setQuantity(conn.resultado.getInt(2));
                order.setSize(String.valueOf(conn.resultado.getInt(3)));
                order.setProductType(String.valueOf(conn.resultado.getInt(4)));
                order.setUnityValue(conn.resultado.getInt(5));

                orderList.add(order);

            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al actualizar pedido \n" + e);
        }
        return orderList;

    }

    @Override
    public List OrderFilter(String dateStart, String dateLimit) {
        List<OrderModel> orderList = new ArrayList<>();

        try {
            SQL = "SELECT  p.id_pedidos, initcap(prov.descripcion), initcap(tp.descripcion), "
                    + "initcap(sh.nombre),  initcap(talla.descripcion), "
                    + "p.cantidad, p.fecha_solicitud, pp.id_precio_productos, "
                    + "p.valor_unidad,  p.valor, initcap(p.estado) "
                    + "FROM PreciosProductos pp "
                    + "join TiposProductos tp on pp.tipo_producto = tp.id_tipo_producto "
                    + "join Editoriales prov on pp.editorial = prov.id_editorial "
                    + "join Pedidos p on p.id_precios_productos = pp.id_precio_productos  "
                    + "join SuperHeroes sh on p.id_super_heroe = sh.id_super_heroe "
                    + "join Tallas talla on p.talla_tamano = talla.id_talla "
                    + " WHERE fecha_solicitud BETWEEN ? and ?"
                    + "ORDER BY p.fecha_solicitud DESC";

            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, dateStart);
            conn.sentencia.setString(2, dateLimit);
            conn.resultado = conn.sentencia.executeQuery();

            while (conn.resultado.next()) {
                OrderModel order = new OrderModel();
                order.setOrderId(conn.resultado.getInt(1));
                order.setSupplier(conn.resultado.getString(2));
                order.setProductType(conn.resultado.getString(3));
                order.setPersonage(conn.resultado.getString(4));
                order.setSize(conn.resultado.getString(5));
                order.setQuantity(conn.resultado.getInt(6));
                order.setApplicationDate(conn.resultado.getString(7));
                order.setPriceProductId(conn.resultado.getInt(8));
                order.setUnityValue(conn.resultado.getInt(9));
                order.setTotalValue(conn.resultado.getInt(10));
                order.setStatus(conn.resultado.getString(11));

                orderList.add(order);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al filtrar la fecha del pedido \n" + e);
        }
        return orderList;
    }

    @Override
    public boolean Cancel(int orderId) {
        try {
            SQL = "DELETE FROM Pedidos "
                    + "WHERE id_pedidos = ?";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setInt(1, orderId);            
            conn.sentencia.executeUpdate();
            statusMessage = true;

        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al actualizar pedido \n" + e);
        }
        return statusMessage;
    }

}
