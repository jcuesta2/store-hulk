/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.modelDao;

import com.cidenet.kardex.configuration.Conexion;
import com.cidenet.kardex.interfaces.General;
import com.cidenet.kardex.model.SizeModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import com.cidenet.kardex.interfaces.Size;

/**
 *
 * @author jcuesta
 */
public class SizeDao implements General, Size {

    String SQL = "";
    String banUpdateDelete = "";
    private boolean statusMessage = false;
    Conexion conn = Conexion.getInstance();

    @Override
    public List List() {
        List<SizeModel> sizeList = new ArrayList<>();
        try {
            SQL = "SELECT id_talla, initcap(descripcion), initcap(estado), initcap(observacion) "
                    + "FROM Tallas "
                    + "WHERE  estado != ? "
                    + "ORDER BY descripcion ASC";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, "eliminado");
            conn.resultado = conn.sentencia.executeQuery();

            while (conn.resultado.next()) {
                SizeModel sizeModel = new SizeModel();

                sizeModel.setSizeId(conn.resultado.getInt(1));
                sizeModel.setDescription(conn.resultado.getString(2));
                sizeModel.setStatus(conn.resultado.getString(3));
                sizeModel.setObservation(conn.resultado.getString(4));

                sizeList.add(sizeModel);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar  productos \n" + e);
        }
        return sizeList;

    }

    @Override
    public List Search(String description) {
        List<SizeModel> sizeList = new ArrayList<>();
        try {
            SQL = "SELECT id_talla, initcap(descripcion), initcap(estado), initcap(observacion) "
                    + "FROM Tallas WHERE descripcion LIKE ? "
                    + "and  estado != ? "
                    + "ORDER BY descripcion ASC";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            String filter = "%".concat(description.toLowerCase()).concat("%");
            conn.sentencia.setString(1, filter);
            conn.sentencia.setString(2, "eliminado");
            conn.resultado = conn.sentencia.executeQuery();

            while (conn.resultado.next()) {
                SizeModel sizeModel = new SizeModel();

                sizeModel.setSizeId(conn.resultado.getInt(1));
                sizeModel.setDescription(conn.resultado.getString(2));
                sizeModel.setStatus(conn.resultado.getString(3));
                sizeModel.setObservation(conn.resultado.getString(4));

                sizeList.add(sizeModel);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar  productos \n" + e);
        }
        return sizeList;

    }

    @Override
    public List AssetList() {
        List<SizeModel> sizeAssetList = new ArrayList<>();
        try {
            SQL = "SELECT id_talla, descripcion "
                    + "FROM Tallas "
                    + "WHERE estado = ? "
                    + "ORDER BY descripcion ASC";

            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, "activo");
            conn.resultado = conn.sentencia.executeQuery();
            while (conn.resultado.next()) {
                SizeModel size = new SizeModel();
                size.setSizeId(conn.resultado.getInt(1));
                size.setDescription(conn.resultado.getString(2));

                sizeAssetList.add(size);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar existencia de personaje \n" + e);
        }
        return sizeAssetList;

    }

    @Override
    public boolean Add(SizeModel talla) {
        if (!SizeValidate(talla)) {
            try {
                SQL = "INSERT INTO Tallas (descripcion,estado,observacion) values (?,?,?)";

                conn.sentencia = conn.conexion.prepareStatement(SQL);
                conn.sentencia.setString(1, talla.getDescription().toLowerCase());
                conn.sentencia.setString(2, talla.getStatus().toLowerCase());
                conn.sentencia.setString(3, talla.getObservation().toLowerCase());

                conn.sentencia.executeUpdate();
                statusMessage = true;
            } catch (SQLException e) {
                JOptionPane.showInputDialog("Error al  insertar talla \n" + e);
            }
        } else {
            statusMessage = banUpdateDelete.equals("UpdateDelete");
        }

        return statusMessage;

    }

    @Override
    public boolean ChangeStatus(int sizeId, String status) {

        SQL = "UPDATE Tallas SET  estado = ? WHERE id_talla = ?";

        try {
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, status);
            conn.sentencia.setInt(2, sizeId);

            conn.sentencia.executeUpdate();
            statusMessage = true;
        } catch (SQLException error) {
            JOptionPane.showInputDialog("Error al cambiar el estado de la talla: \n" + error);

        }
        return statusMessage;

    }

    @Override
    public boolean Edit(SizeModel size) {
        int id = size.getSizeId();
        SizeValidate(size);
        if (id == size.getSizeId()) {
            Update(size);
            statusMessage = true;
        }
        return statusMessage;

    }

    public void Update(SizeModel size) {
        try {
            SQL = "UPDATE Tallas "
                    + "SET  descripcion = ?, estado = ?, observacion = ?"
                    + " WHERE id_talla = ?";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, size.getDescription().toLowerCase());
            conn.sentencia.setString(2, size.getStatus().toLowerCase());
            conn.sentencia.setString(3, size.getObservation().toLowerCase());
            conn.sentencia.setInt(4, size.getSizeId());
            conn.sentencia.executeUpdate();
            statusMessage = true;
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al actualizar la talla : \n" + e);
        }
    }

    public boolean SizeValidate(SizeModel size) {
        try {
            SQL = "SELECT id_talla, estado FROM Tallas WHERE descripcion = ?";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, size.getDescription().toLowerCase());
            conn.resultado = conn.sentencia.executeQuery();

            if (conn.resultado.next()) {
                size.setSizeId(conn.resultado.getInt(1));
                String status = conn.resultado.getString(2);
                if (status.equals("eliminado")) {
                    size.setStatus("activo");
                    Update(size);
                    banUpdateDelete = "UpdateDelete";
                }
                statusMessage = true;
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar existencia de la talla : \n" + e);
        }
        return statusMessage;
    }

}
