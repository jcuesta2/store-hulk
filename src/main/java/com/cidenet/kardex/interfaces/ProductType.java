/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.interfaces;

import com.cidenet.kardex.model.ProductTypeModel;
import java.util.List;

/**
 *
 * @author jcuesta
 */
public interface ProductType {
    public boolean Add(ProductTypeModel product);
    
    public boolean Edit(ProductTypeModel product);
    
    public List AssetList();
    
    public boolean ChangeStatus(ProductTypeModel product);
}
