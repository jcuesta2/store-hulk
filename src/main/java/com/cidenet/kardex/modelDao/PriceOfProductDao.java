/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.modelDao;

import com.cidenet.kardex.configuration.Conexion;
import com.cidenet.kardex.interfaces.General;
import com.cidenet.kardex.interfaces.PriceOfProductType;
import com.cidenet.kardex.model.PriceOfProductModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author jcuesta
 */
public class PriceOfProductDao implements PriceOfProductType, General {

    String SQL, banUpdateDelete = "";
    private boolean statusMessage = false;
    Conexion conn = Conexion.getInstance();

    @Override
    public boolean Add(PriceOfProductModel priceProduct) {
        if (!PriceProductValidate(priceProduct)) {
            try {

                SQL = "INSERT INTO PreciosProductos (tipo_producto,  unidad_precio_proveedor, unidad_precio_tienda, editorial, estado) "
                        + "values (?,?,?,?,?)";

                conn.sentencia = conn.conexion.prepareStatement(SQL);
                conn.sentencia.setInt(1, Integer.parseInt(priceProduct.getProductTypeId()));
                conn.sentencia.setInt(2, priceProduct.getSupplierPrice());
                conn.sentencia.setInt(3, priceProduct.getStorePrice());
                conn.sentencia.setInt(4, Integer.parseInt(priceProduct.getSupplierId()));
                conn.sentencia.setString(5, priceProduct.getStatus());

                conn.sentencia.executeUpdate();
                statusMessage = true;

            } catch (SQLException e) {
                JOptionPane.showInputDialog("Error al registrar precio \n" + e);
            }

        } else {
            statusMessage = banUpdateDelete.equals("UpdateDelete");
        }
        return statusMessage;
    }

    @Override
    public List List() {

        List<PriceOfProductModel> priceList = new ArrayList<>();

        try {
            SQL = "SELECT "
                    + "pp.id_precio_productos, pp.unidad_precio_proveedor, pp.unidad_precio_tienda, initcap(pp.estado), "
                    + "initcap(tp.descripcion), "
                    + "initcap(ed.descripcion) "
                    + "FROM PreciosProductos pp "
                    + "join TiposProductos tp on pp.tipo_producto = tp.id_tipo_producto  "
                    + "join Editoriales ed on pp.editorial = ed.id_editorial "
                    + "WHERE  pp.estado != ? "
                    + "ORDER BY tp.descripcion ASC";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, "eliminado");
            conn.resultado = conn.sentencia.executeQuery();

            while (conn.resultado.next()) {
                PriceOfProductModel price = new PriceOfProductModel();
                price.setPriceOfProductId(conn.resultado.getInt(1));
                price.setSupplierPrice(conn.resultado.getInt(2));
                price.setStorePrice(conn.resultado.getInt(3));
                price.setStatus(conn.resultado.getString(4));
                price.setProductTypeId(conn.resultado.getString(5));
                price.setSupplierId(conn.resultado.getString(6));

                priceList.add(price);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar existencia de pedido \n" + e);
        }
        return priceList;

    }

    @Override
    public List Search(String description) {
        List<PriceOfProductModel> priceList = new ArrayList<>();

        try {
            SQL = "SELECT "
                    + "pp.id_precio_productos, pp.unidad_precio_proveedor, pp.unidad_precio_tienda, initcap(pp.estado), "
                    + "initcap(tp.descripcion), "
                    + "initcap(ed.descripcion) "
                    + "FROM PreciosProductos pp "
                    + "join TiposProductos tp on pp.tipo_producto = tp.id_tipo_producto  "
                    + "join Editoriales ed on pp.editorial = ed.id_editorial "
                    + "where tp.descripcion LIKE ? "
                    + "and  pp.estado != ? "
                    + "ORDER BY tp.descripcion ASC";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            String filter = "%".concat(description.toLowerCase()).concat("%");
            conn.sentencia.setString(1, filter);
            conn.sentencia.setString(2, "eliminado");
            conn.resultado = conn.sentencia.executeQuery();

            while (conn.resultado.next()) {

                PriceOfProductModel price = new PriceOfProductModel();
                price.setPriceOfProductId(conn.resultado.getInt(1));
                price.setSupplierPrice(conn.resultado.getInt(2));
                price.setStorePrice(conn.resultado.getInt(3));
                price.setStatus(conn.resultado.getString(4));
                price.setProductTypeId(conn.resultado.getString(5));
                price.setSupplierId(conn.resultado.getString(6));

                priceList.add(price);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar existencia de precio para el producto \n" + e);
        }
        return priceList;

    }

    @Override
    public boolean ChangeStatus(PriceOfProductModel priceProduct) {

        try {
            SQL = "UPDATE PreciosProductos SET estado = ? WHERE id_precio_productos = ?";
            
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, priceProduct.getStatus().toLowerCase());
            conn.sentencia.setInt(2, priceProduct.getPriceOfProductId());
            conn.sentencia.executeUpdate();
            statusMessage = true;
        } catch (SQLException error) {
            JOptionPane.showInputDialog("Error al cambiar el estado de precio de producto: \n" + error);

        }
        return statusMessage;
    }

    public void Update(PriceOfProductModel priceProduct) {

        try {
            SQL = "UPDATE PreciosProductos "
                    + "SET tipo_producto = ?, "
                    + "unidad_precio_proveedor = ?,  "
                    + "unidad_precio_tienda = ?,  "
                    + "editorial = ?,  "
                    + "estado = ?  "
                    + "WHERE id_precio_productos = ?";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setInt(1, Integer.parseInt(priceProduct.getProductTypeId()));
            conn.sentencia.setInt(2, priceProduct.getSupplierPrice());
            conn.sentencia.setInt(3, priceProduct.getStorePrice());
            conn.sentencia.setInt(4, Integer.parseInt(priceProduct.getSupplierId()));
            conn.sentencia.setString(5, priceProduct.getStatus().toLowerCase());
            conn.sentencia.setInt(6, priceProduct.getPriceOfProductId());

            conn.sentencia.executeUpdate();

        } catch (SQLException error) {
            JOptionPane.showInputDialog("Error al cambiar el estado de precio de producto: \n" + error);

        }
    }

    @Override
    public List ValidatePriceExistence(PriceOfProductModel priceProduct) {
        List<PriceOfProductModel> priceList = new ArrayList<>();
        PriceOfProductModel price = new PriceOfProductModel();
        try {

            SQL = "SELECT id_precio_productos, unidad_precio_proveedor "
                    + " FROM PreciosProductos "
                    + " WHERE editorial = ? and tipo_producto = ?";

            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setInt(1, Integer.parseInt(priceProduct.getSupplierId()));
            conn.sentencia.setInt(2, Integer.parseInt(priceProduct.getProductTypeId()));

            conn.resultado = conn.sentencia.executeQuery();

            if (conn.resultado.next()) {
                price.setPriceOfProductId(conn.resultado.getInt(1));
                price.setSupplierPrice(conn.resultado.getInt(2));
                priceList.add(price);
            } else {
                priceList = null;
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar existencia de pedido \n" + e);
        }
        return priceList;
    }

    @Override
    public boolean Edit(PriceOfProductModel priceProduct) {

        Update(priceProduct);
        statusMessage = true;

        return statusMessage;
    }

    public boolean PriceProductValidate(PriceOfProductModel priceProduct) {
        try {
            SQL = "SELECT id_precio_productos, estado FROM PreciosProductos "
                    + "WHERE  tipo_producto = ? and editorial = ?";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setInt(1, Integer.parseInt(priceProduct.getProductTypeId()));
            conn.sentencia.setInt(2, Integer.parseInt(priceProduct.getSupplierId()));
            conn.resultado = conn.sentencia.executeQuery();
            if (conn.resultado.next()) {
                String status = conn.resultado.getString(2);
                if (status.equals("eliminado")) {
                    priceProduct.setPriceOfProductId(conn.resultado.getInt(1));
                    priceProduct.setStatus("activo");
                    Update(priceProduct);
                    banUpdateDelete = "UpdateDelete";

                }
                statusMessage = true;

            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar existencia de empleado : \n" + e);
        }

        return statusMessage;

    }

}
