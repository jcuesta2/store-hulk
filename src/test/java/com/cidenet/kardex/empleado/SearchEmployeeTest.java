/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.empleado;

import com.cidenet.kardex.model.EmployeeModel;
import com.cidenet.kardex.modelDao.EmployeeDao;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author jcuesta
 */
@RunWith(Parameterized.class)
public class SearchEmployeeTest {

    private final String documentNumber;
    private List<EmployeeModel> listaEsperada = new ArrayList<>();

    public SearchEmployeeTest(String documentNumber, List<EmployeeModel> listaEsperada) {
        this.documentNumber = documentNumber;
        this.listaEsperada = listaEsperada;
    }

    @Parameterized.Parameters
    public static Collection data() {
        List<EmployeeModel> lista = new ArrayList<>();

        lista.add(new EmployeeModel(4, "Pasaporte", "110053563", "Lucia", "Perez", "3124465230", "Activo"));

        return Arrays.asList(new Object[][]{
            {"110", lista}
        });
    }

    @Test
    public void testSearch() {
        try {
            EmployeeDao empDao = new EmployeeDao();
            List<EmployeeModel> resultado = new ArrayList<>();

            resultado = empDao.Search(documentNumber);

            assertThat(resultado, is(listaEsperada));
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }
}
