/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.modelDao;

import com.cidenet.kardex.configuration.Conexion;
import com.cidenet.kardex.interfaces.General;
import com.cidenet.kardex.interfaces.Personage;
import com.cidenet.kardex.model.PersonageModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author jcuesta
 */
public class PersonageDao implements General, Personage {

    String SQL, banUpdateDelete = "";
    private boolean statusMessage = false;
    Conexion conn = Conexion.getInstance();

    @Override
    public List List() {
        List<PersonageModel> employeeList = new ArrayList<>();
        try {
            SQL = "SELECT s.id_super_heroe, initcap(s.nombre), initcap(s.estado), initcap(ed.descripcion) "
                    + "FROM SuperHeroes s join Editoriales ed "
                    + "on s.editorial = ed.id_editorial "
                    + " WHERE  s.estado != ? "
                    + " ORDER BY s.nombre ASC ";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, "eliminado");
            conn.resultado = conn.sentencia.executeQuery();
            while (conn.resultado.next()) {
                PersonageModel personage = new PersonageModel();
                personage.setPersonageId(conn.resultado.getInt(1));
                personage.setName(conn.resultado.getString(2));
                personage.setStatus(conn.resultado.getString(3));
                personage.setSupplier(conn.resultado.getString(4));

                employeeList.add(personage);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar existencia de personaje \n" + e);
        }
        return employeeList;

    }

    @Override
    public List Search(String description) {
        List<PersonageModel> employeeList = new ArrayList<>();
        try {
            SQL = "SELECT s.id_super_heroe, initcap(s.nombre), initcap(s.estado), initcap(ed.descripcion) "
                    + "FROM SuperHeroes s join Editoriales ed "
                    + "on s.editorial = ed.id_editorial WHERE nombre LIKE ? "
                    + "and s.estado != ? "
                    + "ORDER BY ed.descripcion ASC ";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            String filter = "%".concat(description.toLowerCase()).concat("%");
            conn.sentencia.setString(1, filter);
            conn.sentencia.setString(2, "eliminado");
            conn.resultado = conn.sentencia.executeQuery();
            while (conn.resultado.next()) {
                PersonageModel personage = new PersonageModel();
                personage.setPersonageId(conn.resultado.getInt(1));
                personage.setName(conn.resultado.getString(2));
                personage.setStatus(conn.resultado.getString(3));
                personage.setSupplier(conn.resultado.getString(4));

                employeeList.add(personage);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar existencia de personaje \n" + e);
        }
        return employeeList;

    }

    @Override
    public List AssetList() {
        List<PersonageModel> perAssetList = new ArrayList<>();
        try {
            SQL = "SELECT id_super_heroe, initcap(nombre) "
                    + "FROM SuperHeroes "
                    + "WHERE estado = ? "
                    + "ORDER BY nombre ASC";

            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, "activo");
            conn.resultado = conn.sentencia.executeQuery();
            while (conn.resultado.next()) {
                PersonageModel personage = new PersonageModel();
                personage.setPersonageId(conn.resultado.getInt(1));
                personage.setName(conn.resultado.getString(2));

                perAssetList.add(personage);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar existencia de personaje \n" + e);
        }
        return perAssetList;

    }

    @Override
    public boolean Add(PersonageModel personage) {
        if (!PersonageValidate(personage)) {
            try {

                SQL = "INSERT INTO SuperHeroes (editorial, nombre, estado) values (?,?,?)";

                conn.sentencia = conn.conexion.prepareStatement(SQL);
                conn.sentencia.setInt(1, Integer.parseInt(personage.getSupplier()));
                conn.sentencia.setString(2, personage.getName().toLowerCase());
                conn.sentencia.setString(3, personage.getStatus().toLowerCase());

                conn.sentencia.executeUpdate();
                statusMessage = true;

            } catch (SQLException e) {
                JOptionPane.showInputDialog("Error al registrar personaje \n" + e);
            }
        } else {
            statusMessage = banUpdateDelete.equals("UpdateDelete");
        }

        return statusMessage;
    }

    @Override
    public boolean ChangeStatus(PersonageModel personage) {
        try {
            SQL = "UPDATE SuperHeroes SET estado = ? WHERE id_super_heroe = ?";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, personage.getStatus().toLowerCase());
            conn.sentencia.setInt(2, personage.getPersonageId());
            conn.sentencia.executeUpdate();
            statusMessage = true;
        } catch (SQLException error) {
            JOptionPane.showInputDialog("Error al cambiar el estado del producto: \n" + error);

        }
        return statusMessage;
    }

    @Override
    public boolean Edit(PersonageModel personage) {
        int id = personage.getPersonageId();
        PersonageValidate(personage);
            if (id == personage.getPersonageId()) {
                Update(personage);
                statusMessage = true;
            }

        return statusMessage;

    }

    public void Update(PersonageModel personage) {
        try {
            SQL = "UPDATE SuperHeroes "
                    + "SET  editorial = ?, nombre = ?, estado = ?"
                    + " WHERE id_super_heroe = ?";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setInt(1, Integer.parseInt(personage.getSupplier()));
            conn.sentencia.setString(2, personage.getName().toLowerCase());
            conn.sentencia.setString(3, personage.getStatus().toLowerCase());
            conn.sentencia.setInt(4, personage.getPersonageId());
            conn.sentencia.executeUpdate();
            statusMessage = true;
        } catch (SQLException error) {
            JOptionPane.showInputDialog("Error al actualizar los datos : \n" + error);

        }
    }

    public boolean PersonageValidate(PersonageModel personage) {
        try {
            SQL = "SELECT id_super_heroe, estado FROM SuperHeroes WHERE nombre = ?";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, personage.getName().toLowerCase());
            conn.resultado = conn.sentencia.executeQuery();

            if (conn.resultado.next()) {
                personage.setPersonageId(conn.resultado.getInt(1));
                String status = conn.resultado.getString(2);
                if (status.equals("eliminado")) {
                    personage.setStatus("activo");
                    Update(personage);
                    banUpdateDelete = "UpdateDelete";
                }
                statusMessage = true;
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar existencia de la talla : \n" + e);
        }
        return statusMessage;
    }

}
