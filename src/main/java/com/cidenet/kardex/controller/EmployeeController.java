/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.controller;

import com.cidenet.kardex.model.DocumentModel;
import com.cidenet.kardex.model.EmployeeModel;
import com.cidenet.kardex.modelDao.DocumentTypeDao;
import com.cidenet.kardex.modelDao.EmployeeDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

/**
 *
 * @author jcuesta
 */
@WebServlet(name = "EmployeeController", urlPatterns = {"/EmployeeController"})
public class EmployeeController extends HttpServlet {

    List<EmployeeModel> employeeList, EditModal = new ArrayList<>();
    EmployeeModel employeeModel = new EmployeeModel();

    List<DocumentModel> documentList = new ArrayList<>();
    DocumentTypeDao document = new DocumentTypeDao();

    EmployeeDao employee = new EmployeeDao();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EmployeeController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EmployeeController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String codeStatus = "";
        String message = "";
        String openModal = "";
        String employeeToSearch = "";
        int employeeId;
        String action = request.getParameter("action");

        switch (action) {
            case "EmployeeList": {
                
            }
            break;
            case "SearchEmployee": {
                employeeToSearch = request.getParameter("txtsearchValue");
                
            }
            break;
            case "ListActiveDocument": {
                openModal = "Add";
                documentList = document.AssetList();
                request.setAttribute("documentList", documentList);

            }
            break;
            case "StatusChange": {
                int resp = JOptionPane.showConfirmDialog(null, "¿Está seguro que desea eliminar el empleado? ", "Alerta!", JOptionPane.YES_NO_OPTION);
                if (resp == 0) {

                    employeeId = Integer.parseInt(request.getParameter("employeeId"));
                    
                    if (employee.ChangeStatus(employeeId, "eliminado")) {
                        codeStatus = "Success";
                        message = "Empleado eliminado correctamente";
                    } else {
                        codeStatus = "Error";
                        message = "No se pudo eliminar el empleado";
                    }

                } else {
                    codeStatus = "Información";
                    message = "No se realizaron cambios";
                }
            }
            break;
            case "EditModal": {
                employeeId = Integer.parseInt(request.getParameter("employeeId"));
                for (EmployeeModel employeeItem : employeeList) {
                    EditModal.clear();
                    if (employeeId == employeeItem.getEmployeeId()) {
                        openModal = "Edit";
                        EditModal.add(employeeItem);
                        documentList = document.AssetList();
                        request.setAttribute("documentList", documentList);
                        request.setAttribute("EditModal", EditModal);
                        break;
                    }
                }

            }
            break;
        }
        if (action.equals("SearchEmployee")) {
            employeeList = employee.Search(employeeToSearch);
        }else{
            employeeList = employee.List();
        }
        request.setAttribute("codeStatus", codeStatus);
        request.setAttribute("message", message);
        request.setAttribute("openModal", openModal);
        request.setAttribute("employeeList", employeeList);
        request.getRequestDispatcher("com.cidenet.kardex.view/employee.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String codeStatus = "";
        String message = "";
        String action = request.getParameter("action");
        switch (action) {
            case "EmployeeAdd": {
                employeeModel.setDocumentType(request.getParameter("txttypeDocument"));
                employeeModel.setDocumentNumber(request.getParameter("txtdocumentNumber"));
                employeeModel.setName(request.getParameter("txtname"));
                employeeModel.setLastname(request.getParameter("txtsurname"));
                employeeModel.setContact(request.getParameter("txtcontact"));
                employeeModel.setStatus(request.getParameter("txtstatus"));

                if (employee.Add(employeeModel)) {
                    codeStatus = "Success";
                    message = "Se ha guardado correctamente";
                } else {
                    codeStatus = "Error";
                    message = "Empleado ya existe";
                }
                request.setAttribute("codeStatus", codeStatus);
                request.setAttribute("message", message);
            }
            break;
            case "EmployeeEdit": {
                
                employeeModel.setDocumentType(request.getParameter("txttypeDocument"));
                employeeModel.setEmployeeId(Integer.parseInt(request.getParameter("txtemployeeId")));
                employeeModel.setDocumentNumber(request.getParameter("txtdocumentNumber"));
                employeeModel.setName(request.getParameter("txtname"));
                employeeModel.setLastname(request.getParameter("txtsurname"));
                employeeModel.setContact(request.getParameter("txtcontact"));
                employeeModel.setStatus(request.getParameter("txtstatus"));

                if (employee.Edit(employeeModel)) {
                    codeStatus = "Success";
                    message = "Se ha guardado correctamente";
                } else {
                    codeStatus = "Error";
                    message = "Empleado ya existe";
                }
                request.setAttribute("codeStatus", codeStatus);
                request.setAttribute("message", message);
            }
            break;
        }
        employeeList = employee.List();
        request.setAttribute("employeeList", employeeList);
        request.getRequestDispatcher("com.cidenet.kardex.view/employee.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
