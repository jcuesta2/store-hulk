<%-- 
    Document   : supplier
    Created on : Nov 19, 2019, 9:33:48 AM
    Author     : jcuesta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <style>

            .table-fixed tbody {
                height: 300px;
                overflow-y: auto;
                width: 100%;
            }

            .table-fixed thead,
            .table-fixed tbody,
            .table-fixed tr,
            .table-fixed td,
            .table-fixed th {
                display: block;
            }

            .table-fixed tbody td,
            .table-fixed tbody th,
            .table-fixed thead > tr > th {
                float: left;
                position: relative;

                &::after {
                    content: '';
                    clear: both;
                    display: block;
                }
            }



            body {
                background: #74ebd5;
                background: -webkit-linear-gradient(to right, #74ebd5, #ACB6E5);
                background: linear-gradient(to right, #74ebd5, #ACB6E5);
                min-height: 100vh;

            }


        </style>

    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">
                <img src="/docs/4.3/assets/brand/bootstrap-solid.svg" width="30" height="30" alt="">
                Tienda-COMICS
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="../index.jsp">Inicio <span class="sr-only">(current)</span></a>
                    </li>  
                    <li class="nav-item">
                        <a class="nav-link" href="StoreController?action=StoreList">Tienda</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="SalesController?action=SaleList">Ventas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="StorageController?action=StorageList">Bodega</a>
                    </li>          
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Configuraciones
                        </a>                        
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="EmployeeController?action=EmployeeList">Empleados</a>
                            <a class="dropdown-item" href="PersonageController?action=PersonageList">Personajes</a>
                            <a class="dropdown-item" href="SupplierController?action=SupplierList" title="Clic para listar">Proveedores</a>
                            <a class="dropdown-item" href="ProductTypeController?action=ProductTypeList">Tipos de productos</a>
                            <a class="dropdown-item" href="SizeController?action=SizeList">Tallas o tamaños</a>
                            <a class="dropdown-item" href="PriceOfProductController?action=PriceList">Cotización final de productos</a>
                        </div>
                    </li>

                </ul>
            </div>
        </nav>
        <div>
            <nav class="navbar navbar-dark" >

                <div class="row">
                    <div class="col-md-3 col-md-offset-9 text-right" >
                        <div class="row">
                            <div class="col-md-3 col-md-offset-9 text-right" >
                                <div class="btn-group" role="group">                            
                                    <a class="btn btn-primary my-2 my-sm-0" href="OrderController?action=JoinToSelect" title="Click para agregar">Nuevo</a>
                                </div>
                            </div>
                        </div>          
                    </div>
                </div>                 
                <form action="OrderController" method="GET" class="form-inline">
                    <label class=" my-2 my-sm-0" style="color:  red">*</label>&nbsp;<span class="glyphicon glyphicon-step-forward"></span>
                    <input type="date" name="txtDateStart" class="form-control mr-sm-2"  placeholder="Fecha inicial"   title="Pedidos desde..." required> 
                    <input type="date" name="txtDateLimit" class="form-control mr-sm-2"  placeholder="Fecha limite"   title="Pedidos hasta..." > 
                    <button type="submit" name="action" class="btn btn-primary my-2 my-sm-0"  value="OrderFilter" title="Click para buscar">Buscar</button>&nbsp;<span class="glyphicon glyphicon-step-forward"></span>
                    <a class="btn btn-primary my-2 my-sm-0" role="button" href="OrderController?action=OrderList" data-toggle="tooltip" data-placement="right" title="Click para listar">
                        Ver Todos
                    </a>&nbsp;<span class="glyphicon glyphicon-step-forward"></span>
                </form>
            </nav>
            <h4 align=center>Lista De Pedidos</h4>
            <div class="container py-5">
                <div class="row">
                    <div class="col-md-12  mx-auto bg-white  rounded shadow"> <!-- Aqui esta el detalle-->                                        
                        <!-- Fixed header table-->
                        <div class="table-responsive">
                            <table class="table table-fixed">                            
                                <thead>                                   
                                    <tr>     
                                        <th scope="col" class="col-2">Proveedor</th>
                                        <th scope="col" class="col-3">Descripcion</th>                                                                                
                                        <th scope="col" class="col-2">Fecha</th> 
                                        <th scope="col" class="col-2">Valor c/u - Total</th>                                                                                
                                        <th scope="col"class="col-3">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test="${orderList != null && not empty orderList}">
                                        <c:forEach var="order" items="${orderList}">
                                            <tr>
                                                <td  width="10%" style="display: none;" scope="row">${order.orderId}</td>
                                                <td  width="10%" style="display: none;" scope="row">${order.priceProductId}</td> 
                                                <td class="col-2">${order.supplier}</td> 
                                                <td class="col-3">${order.quantity} ${order.productType} - ${order.personage} - ${order.size}</td>                                                                                                 
                                                <td class="col-2">${order.applicationDate}</td>                    
                                                <td class="col-2">${order.unityValue} - ${order.totalValue}</td>                                                                                                 
                                                <td class="col-3">
                                                    <c:choose>
                                                        <c:when test="${order.status == 'Entregado'}">
                                                            <a id="btnReceive" class="btn btn-secondary" role="button" href="" data-toggle="tooltip" data-placement="right" title="Pedido ya fue entregado">Entregado</a>                                
                                                        </c:when>
                                                        <c:otherwise>
                                                            <a id="btnReceive" class="btn btn-primary" role="button" href="OrderController?action=Receive&orderId=${order.orderId}" data-toggle="tooltip" data-placement="right" title="Click para recibir">Recibir</a>  
                                                            <a id="btnCancelar" class="btn btn-danger" role="button" href="OrderController?action=Cancel&orderId=${order.orderId}"  data-placement="right" title="Click para Editar">Cancelar</a>                                    
                                                        </c:otherwise>
                                                    </c:choose>
                                                </td>                                                                                                  
                                            </tr>
                                        </c:forEach>
                                    </c:if>                        
                                </tbody>
                            </table>
                        </div>
                        <!-- Modal Crear pedido -->    
                        <c:if test="${openModal == 'Yes'}">
                            <div class="modal fade" id="ModalPedido" tabindex="-1" role="dialog" aria-labelledby="ModalPedido" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div id="divModalHeadCrear" class="modal-header">
                                            <h5 class="modal-title" id="ModalPedidoHead">Realizar pedido</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form  id="ModalPedidoForm" action="OrderController" method="POST">
                                                <div class="form-group">                                   
                                                    <label style="color: red" for="message-text" class="col-form-label">* <label style="color: black" for="message-text" class="col-form-label">Producto :</label></label>
                                                    <select class="custom-select" id="txtproduct" name="txtproduct" placeholder="Selecione el producto...." required> 
                                                        <option value="" selected>Selecciona el tipo de producto....</option>
                                                        <c:forEach var="producto" items="${proAssetList}">
                                                            <option value="${producto.productTypeId}">${producto.description}</option>
                                                        </c:forEach>                                             
                                                    </select>
                                                </div>
                                                <div class="form-group">                                   
                                                    <label style="color: red" for="message-text" class="col-form-label">* <label style="color: black" for="message-text" class="col-form-label">Proveedor :</label></label>
                                                    <select class="custom-select" id="txtpersonage" name="txtsupplier" title="Selecione el Personaje...." required>
                                                        <option value="" selected>Selecciona el proveedor....</option>
                                                        <c:forEach var="supplier" items="${supplierAssetList}">
                                                            <option value="${supplier.editorialId}">${supplier.description}</option>
                                                        </c:forEach>                                             
                                                    </select>
                                                </div>
                                                <div class="form-group">                                   
                                                    <label style="color: red" for="message-text" class="col-form-label">* <label style="color: black" for="message-text" class="col-form-label">Personaje :</label></label>
                                                    <select class="custom-select" id="txtpersonage" name="txtpersonage" placeholder="Selecione el Personaje...." required>
                                                        <option value="" selected>Selecciona el personaje....</option>
                                                        <c:forEach var="personage" items="${personageAssetList}">
                                                            <option value="${personage.personageId}">${personage.name}</option>
                                                        </c:forEach>                                             
                                                    </select>
                                                </div>
                                                <div class="form-group">                                   
                                                    <label style="color: red" for="message-text" class="col-form-label">* <label style="color: black" for="message-text" class="col-form-label">Talla :</label></label>
                                                    <select class="custom-select" id="txtsize" name="txtsize" placeholder="Selecione la talla...." required>
                                                        <option value="" selected>Selecciona la talla....</option>
                                                        <c:forEach var="size" items="${sizeAssetList}">
                                                            <option value="${size.sizeId}">${size.description}</option>
                                                        </c:forEach>                                             
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label style="color: red" for="message-text" class="col-form-label">* <label style="color: black" for="message-text" class="col-form-label">Cantidad :</label></label>
                                                    <input type=number class="form-control"  id="txtquantity" name="txtquantity" maxlength="4"  title="Cantidad de pedido" required>      
                                                </div>                                    
                                                <div class="modal-footer">
                                                    <button id="close" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                    <button id="btn-guardar" type="submit"  class="btn btn-info" name="action" value="OrderAdd">Guardar</button>
                                                </div>
                                            </form>   
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:if>            
                        <!-- Modal Response -->
                        <c:if test="${not empty codeStatus }">
                            <div class="container">
                                <div class="modal fade" id="ModalResponse" role="dialog">
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content"> 
                                            <div id="divModalHeadConf"  class="modal-header">
                                                <h4 class="modal-title">${codeStatus}</h4>                        
                                            </div>
                                            <div style="alignment-baseline: central" class="modal-body">
                                                <p>${message}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <c:if test="${codeStatus == 'Alerta' }">
                                                    <a class="btn btn-info my-2 my-sm-0" href="PriceOfProductController?action=PriceList" title="Click para ingresar el valor">Ir a precios productos</a>
                                                </c:if>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:if>
                        <script>
                            $("#ModalPedido").modal("show");
                            $("#ModalResponse").modal("show");
                            $("#ModalPedidoEdit").modal("show");

                        </script>
                        </body>
                        </html>
