/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.model;

/**
 *
 * @author jcuesta
 */
public class DocumentModel {
    private int documentTypeId;
    private String description;
    private String status;

    @Override
    public String toString() {
        return "DocumentModel{" + "documentTypeId=" + documentTypeId + ", description=" + description + ", status=" + status + '}';
    }

    public int getDocumentTypeId() {
        return documentTypeId;
    }

    public void setDocumentTypeId(int documentTypeId) {
        this.documentTypeId = documentTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DocumentModel(int documentTypeId, String description, String status) {
        this.documentTypeId = documentTypeId;
        this.description = description;
        this.status = status;
    }

    public DocumentModel() {
    }
    
}
