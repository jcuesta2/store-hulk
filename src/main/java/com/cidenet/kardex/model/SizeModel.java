/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.model;

/**
 *
 * @author jcuesta
 */
public class SizeModel {

    private int sizeId;
    private String description;
    private String observation;

    @Override
    public String toString() {
        return "TallaModel{" + "sizeId=" + sizeId + ", description=" + description + ", observation=" + observation + ", status=" + status + '}';
    }

    public int getSizeId() {
        return sizeId;
    }

    public void setSizeId(int sizeId) {
        this.sizeId = sizeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public SizeModel(int sizeId, String description, String observation, String status) {
        this.sizeId = sizeId;
        this.description = description;
        this.observation = observation;
        this.status = status;
    }

    public SizeModel(String description, String observation, String status) {

        this.description = description;
        this.observation = observation;
        this.status = status;
    }

    public SizeModel() {
    }
    private String status;
}
