/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.modelDao;

import com.cidenet.kardex.configuration.Conexion;
import com.cidenet.kardex.interfaces.General;
import com.cidenet.kardex.interfaces.ProductType;
import com.cidenet.kardex.model.ProductTypeModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author jcuesta
 */
public class ProductTypeDao implements General, ProductType {

    String SQL, banUpdateDelete = "";
    private boolean statusMessage = false;
    Conexion conn = Conexion.getInstance();

    @Override
    public List List() {

        List<ProductTypeModel> productTypeList = new ArrayList<>();
        try {
            SQL = "SELECT id_tipo_producto, initcap(descripcion), initcap(estado) "
                    + "FROM TiposProductos "
                    + "WHERE  estado != ? "
                    + "ORDER BY descripcion ASC";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, "eliminado");
            conn.resultado = conn.sentencia.executeQuery();

            while (conn.resultado.next()) {
                ProductTypeModel productTypeModel = new ProductTypeModel();

                productTypeModel.setProductTypeId(conn.resultado.getInt(1));
                productTypeModel.setDescription(conn.resultado.getString(2));
                productTypeModel.setStatus(conn.resultado.getString(3));

                productTypeList.add(productTypeModel);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar  productos \n" + e);
        }
        return productTypeList;

    }

    @Override
    public List Search(String description) {
        List<ProductTypeModel> productTypeList = new ArrayList<>();
        try {
            SQL = "SELECT id_tipo_producto, initcap(descripcion), initcap(estado) "
                    + "FROM TiposProductos WHERE descripcion LIKE ? "
                    + "and  estado != ? "
                    + "ORDER BY descripcion ASC";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            String filter = "%".concat(description.toLowerCase()).concat("%");
            conn.sentencia.setString(1, filter);
            conn.sentencia.setString(2, "eliminado");
            conn.resultado = conn.sentencia.executeQuery();

            while (conn.resultado.next()) {
                ProductTypeModel productTypeModel = new ProductTypeModel();

                productTypeModel.setProductTypeId(conn.resultado.getInt(1));
                productTypeModel.setDescription(conn.resultado.getString(2));
                productTypeModel.setStatus(conn.resultado.getString(3));

                productTypeList.add(productTypeModel);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar  productos \n" + e);
        }
        return productTypeList;
    }

    @Override
    public List AssetList() {
        List<ProductTypeModel> proAssetList = new ArrayList<>();
        try {
            SQL = "SELECT id_tipo_producto, initcap(descripcion) "
                    + "FROM TiposProductos "
                    + "WHERE estado = ?";

            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, "activo");
            conn.resultado = conn.sentencia.executeQuery();
            while (conn.resultado.next()) {
                ProductTypeModel product = new ProductTypeModel();
                product.setProductTypeId(conn.resultado.getInt(1));
                product.setDescription(conn.resultado.getString(2));

                proAssetList.add(product);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar existencia de personaje \n" + e);
        }
        return proAssetList;

    }

    @Override
    public boolean Add(ProductTypeModel product) {
        if (!ProductValidate(product)) {
            try {
                SQL = "INSERT INTO TiposProductos (descripcion,estado) values (?,?)";

                conn.sentencia = conn.conexion.prepareStatement(SQL);
                conn.sentencia.setString(1, product.getDescription().toLowerCase());
                conn.sentencia.setString(2, product.getStatus().toLowerCase());

                conn.sentencia.executeUpdate();
                statusMessage = true;
            } catch (SQLException e) {
                JOptionPane.showInputDialog("Error al registrar el  producto : \n" + e);
            }
        } else {
            statusMessage = banUpdateDelete.equals("UpdateDelete");
        }

        return statusMessage;

    }

    @Override
    public boolean ChangeStatus(ProductTypeModel product) {

        SQL = "UPDATE TiposProductos SET estado = ?  WHERE id_tipo_producto = ?";

        try {
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, product.getStatus());
            conn.sentencia.setInt(2, product.getProductTypeId());
            conn.sentencia.executeUpdate();
            statusMessage = true;
        } catch (SQLException error) {
            JOptionPane.showInputDialog("Error al cambiar el estado del producto: \n" + error);

        }
        return statusMessage;
    }

    @Override
    public boolean Edit(ProductTypeModel product) {
        int id = product.getProductTypeId();
        ProductValidate(product);
        if (id == product.getProductTypeId()) {
            Update( product );
            statusMessage = true;
        }
        return statusMessage;

    }

    public void Update(ProductTypeModel product) {
        try {
            SQL = "UPDATE TiposProductos "
                    + "SET  descripcion = ?, estado = ?"
                    + " WHERE id_tipo_producto = ?";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, product.getDescription().toLowerCase());
            conn.sentencia.setString(2, product.getStatus().toLowerCase());
            conn.sentencia.setInt(3, product.getProductTypeId());
            conn.sentencia.executeUpdate();            
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al actualizar el  producto : \n" + e);
        }
    }

    public boolean ProductValidate(ProductTypeModel product) {
        try {
            SQL = "SELECT id_tipo_producto, estado FROM TiposProductos WHERE descripcion = ?";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, product.getDescription().toLowerCase());
            conn.resultado = conn.sentencia.executeQuery();

            if (conn.resultado.next()) {
                product.setProductTypeId(conn.resultado.getInt(1));
                String status = conn.resultado.getString(2);
                if (status.equals("eliminado")) {
                    product.setStatus("activo");
                    Update(product);
                    banUpdateDelete = "UpdateDelete";
                }
                statusMessage = true;
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar existencia del producto : \n" + e);
        }
        return statusMessage;
    }

}
