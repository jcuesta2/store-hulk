/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.interfaces;

import com.cidenet.kardex.model.SizeModel;
import java.util.List;

/**
 *
 * @author jcuesta
 */
public interface Size {

    public boolean Add(SizeModel talla);

    public boolean Edit(SizeModel talla);
    
    public List AssetList();
    
    public boolean ChangeStatus(int sizeId, String status);

}
