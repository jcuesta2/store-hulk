/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.model;

/**
 *
 * @author jcuesta
 */
public class PriceOfProductModel {

    private int priceOfProductId;
    private String productTypeId;
    private int supplierPrice;
    private int storePrice;
    private String supplierId;
    private String status;

    public PriceOfProductModel() {
    }

    @Override
    public String toString() {
        return "PriceOfProductModel{" + "priceOfProductId=" + priceOfProductId + ", productTypeId=" + productTypeId + ", supplierPrice=" + supplierPrice + ", storePrice=" + storePrice + ", supplierId=" + supplierId + ", status=" + status + '}';
    }

    public PriceOfProductModel(int priceOfProductId, String productTypeId, int supplierPrice, int storePrice, String supplierId, String status) {
        this.priceOfProductId = priceOfProductId;
        this.productTypeId = productTypeId;
        this.supplierPrice = supplierPrice;
        this.storePrice = storePrice;
        this.supplierId = supplierId;
        this.status = status;
    }

    public int getPriceOfProductId() {
        return priceOfProductId;
    }

    public void setPriceOfProductId(int priceOfProductId) {
        this.priceOfProductId = priceOfProductId;
    }

    public String getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(String productTypeId) {
        this.productTypeId = productTypeId;
    }

    public int getSupplierPrice() {
        return supplierPrice;
    }

    public void setSupplierPrice(int supplierPrice) {
        this.supplierPrice = supplierPrice;
    }

    public int getStorePrice() {
        return storePrice;
    }

    public void setStorePrice(int storePrice) {
        this.storePrice = storePrice;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
