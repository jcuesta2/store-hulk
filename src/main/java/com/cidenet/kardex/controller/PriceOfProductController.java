/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.controller;

import com.cidenet.kardex.model.PriceOfProductModel;
import com.cidenet.kardex.model.ProductTypeModel;
import com.cidenet.kardex.model.SupplierModel;
import com.cidenet.kardex.modelDao.PriceOfProductDao;
import com.cidenet.kardex.modelDao.ProductTypeDao;
import com.cidenet.kardex.modelDao.SupplierDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

/**
 *
 * @author jcuesta
 */
@WebServlet(name = "PriceOfProductController", urlPatterns = {"/PriceOfProductController"})
public class PriceOfProductController extends HttpServlet {

    List<SupplierModel> supplierList = new ArrayList<>();
    SupplierDao supplier = new SupplierDao();

    List<ProductTypeModel> productList = new ArrayList<>();
    ProductTypeDao product = new ProductTypeDao();

    List<PriceOfProductModel> priceList, EditModal = new ArrayList<>();
    PriceOfProductDao price = new PriceOfProductDao();
    PriceOfProductModel priceModel = new PriceOfProductModel();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PriceOfProductController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PriceOfProductController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int priceOfProductId;
        String codeStatus = "";
        String message = "";
        String openModal = "";
        String priceOfProductToSearch = "";
        String action = request.getParameter("action");
        switch (action) {
            case "PriceList": {
               
            }
            break;
            case "ListToPrice": {
                openModal = "Yes";
                productList = product.List();
                supplierList = supplier.List();

                request.setAttribute("openModal", openModal);
                request.setAttribute("productList", productList);
                request.setAttribute("supplierList", supplierList);

            }
            break;
            case "SearchPrice": {
                 priceOfProductToSearch = request.getParameter("txtsearchValue");
                
            }
            break;
            case "StatusChange": {
                int resp = JOptionPane.showConfirmDialog(null, "¿Está seguro que desea eliminar el precio asignado al producto? ", "Alerta!", JOptionPane.YES_NO_OPTION);
                if (resp == 0) {
                    
                    priceOfProductId = Integer.parseInt(request.getParameter("priceOfProductId"));
                    priceModel.setPriceOfProductId(priceOfProductId);
                    priceModel.setStatus("eliminado");
                    if (price.ChangeStatus(priceModel)) {
                        codeStatus = "Success";
                        message = "Precio eliminado correctamente";
                    } else {
                        codeStatus = "Error";
                        message = "No se pudo eliminar el Precio";
                    }

                } else {
                    codeStatus = "Información";
                    message = "No se realizaron cambios";
                }
            }
            break;
            case "EditModal": {
                priceOfProductId = Integer.parseInt(request.getParameter("priceOfProductId"));
                for (PriceOfProductModel priceItem : priceList) {
                    EditModal.clear();                    
                    if (priceOfProductId == priceItem.getPriceOfProductId()) {
                        openModal = "Edit";
                        EditModal.add(priceItem);

                        productList = product.List();
                        supplierList = supplier.List();
                        String productToEdit = priceItem.getProductTypeId();
                        String supplierToEdit = priceItem.getSupplierId();
                        ProductTypeModel editModalProduct = new ProductTypeModel();
                        SupplierModel editModalSupplier = new SupplierModel();

                        for (ProductTypeModel productTypeItem : productList) {
                            if (productToEdit.equals(productTypeItem.getDescription())) {
                                
                                editModalProduct.setProductTypeId(productTypeItem.getProductTypeId());
                                editModalProduct.setDescription(productToEdit);
                                break;
                            }
                        }
                        for (SupplierModel supplierItem : supplierList) {
                            if (supplierToEdit.equals(supplierItem.getDescription())) {
                                
                                editModalSupplier.setEditorialId(supplierItem.getEditorialId());
                                editModalSupplier.setDescription(supplierToEdit);
                                break;
                            }
                        } 
                        productList.clear();
                        supplierList.clear();
                        productList.add(editModalProduct);
                        supplierList.add(editModalSupplier);
                        request.setAttribute("openModal", openModal);
                        request.setAttribute("EditModal", EditModal);
                        request.setAttribute("productList", productList);
                        request.setAttribute("supplierList", supplierList);
                        break;
                    }
                }

            }
            break;

        }
        
        if (action.equals("SearchPrice")) {
            priceList = price.Search(priceOfProductToSearch);
        }else{
             priceList = price.List();
        }

        request.setAttribute("codeStatus", codeStatus);
        request.setAttribute("message", message);
        request.setAttribute("priceList", priceList);
        request.getRequestDispatcher("com.cidenet.kardex.view/priceOfProduct.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String codeStatus = "";
        String message = "";

        String action = request.getParameter("action");
        switch (action) {
            case "priceAdd": {
                priceModel.setProductTypeId(request.getParameter("txtproduct"));
                priceModel.setSupplierPrice(Integer.parseInt(request.getParameter("txtsupplierprice")));
                priceModel.setStorePrice(Integer.parseInt(request.getParameter("txtstoreprice")));
                priceModel.setSupplierId(request.getParameter("txteditorial"));
                priceModel.setStatus(request.getParameter("txtstatus"));

                if (price.Add(priceModel)) {
                    codeStatus = "Success";
                    message = "Se ha guardado correctamente";
                } else {
                    codeStatus = "Error";
                    message = "Precio de producto ya existe";
                }

            }
            break;
            case "priceEdit": {

                priceModel.setProductTypeId(request.getParameter("txtproduct"));
                priceModel.setSupplierPrice(Integer.parseInt(request.getParameter("txtsupplierprice")));
                priceModel.setStorePrice(Integer.parseInt(request.getParameter("txtstoreprice")));
                priceModel.setSupplierId(request.getParameter("txteditorial"));
                priceModel.setStatus(request.getParameter("txtstatus"));
                priceModel.setPriceOfProductId(Integer.parseInt(request.getParameter("txtpriceId")));

                if (price.Edit(priceModel)) {
                    codeStatus = "Success";
                    message = "Se ha guardado correctamente";
                } else {
                    codeStatus = "Error";
                    message = "Empleado ya existe";
                }

            }
            break;
        }
        priceList = price.List();
        request.setAttribute("priceList", priceList);
        request.setAttribute("codeStatus", codeStatus);
        request.setAttribute("message", message);
        request.getRequestDispatcher("com.cidenet.kardex.view/priceOfProduct.jsp").forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
