/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.controller;

import com.cidenet.kardex.model.SizeModel;
import com.cidenet.kardex.modelDao.SizeDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

/**
 *
 * @author jcuesta
 */
@WebServlet(name = "SizeController", urlPatterns = {"/SizeController"})
public class SizeController extends HttpServlet {

    SizeModel sizeModel = new SizeModel();
    List<SizeModel> sizeList, EditModal = new ArrayList<>();
    SizeDao size = new SizeDao();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SizeController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SizeController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int sizeId;
        String codeStatus = "";
        String message = "";
        String sizeToSearch = "";
        String openModal = "";
        String action = request.getParameter("action");
        switch (action) {
            case "SizeList": {

            }
            break;
            case "SearchSize": {
                sizeToSearch = request.getParameter("txtsearchValue");

            }
            break;
            case "StatusChange": {
                int resp = JOptionPane.showConfirmDialog(null, "¿Está seguro que desea eliminar la talla? ", "Alerta!", JOptionPane.YES_NO_OPTION);
                if (resp == 0) {

                    sizeId = Integer.parseInt(request.getParameter("sizeId"));
                    String status = "Eliminado";
                    
                    if (size.ChangeStatus(sizeId, status)) {
                        codeStatus = "Success";
                        message = "Talla eliminada correctamente";
                    } else {
                        codeStatus = "Error";
                        message = "No se pudo eliminar talla";
                    }

                } else {
                    codeStatus = "Información";
                    message = "No se realizaron cambios";
                }
            }
            break;
            case "EditModal": {
                sizeId = Integer.parseInt(request.getParameter("sizeId"));
                for (SizeModel sizeItem : sizeList) {
                    EditModal.clear();
                    if (sizeId == sizeItem.getSizeId()) {
                        openModal = "Edit";
                        EditModal.add(sizeItem);
                        request.setAttribute("EditModal", EditModal);
                        break;
                    }
                }

            }
            break;
        }
        if (action.equals("SearchSize")) {
            sizeList = size.Search(sizeToSearch);
        } else {
            sizeList = size.List();
        }
        request.setAttribute("sizeList", sizeList);
        request.setAttribute("codeStatus", codeStatus);
        request.setAttribute("message", message);
        request.setAttribute("openModal", openModal);
        request.getRequestDispatcher("com.cidenet.kardex.view/size.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String codeStatus = "";
        String message = "";
        String action = request.getParameter("action");

        switch (action) {
            case "SizeAdd": {
                sizeModel.setDescription(request.getParameter("txtsize"));
                sizeModel.setStatus(request.getParameter("txtstatus"));
                sizeModel.setObservation(request.getParameter("txtobservation"));

                if (size.Add(sizeModel)) {
                    codeStatus = "Success";
                    message = "Se ha guardado correctamente";
                } else {
                    codeStatus = "Error";
                    message = "Talla ya existe";
                }
               
            }
            break;
            case "SizeEdit": {
                sizeModel.setSizeId(Integer.parseInt(request.getParameter("txtsizetId")));
                sizeModel.setDescription(request.getParameter("txtsize"));
                sizeModel.setStatus(request.getParameter("txtstatus"));
                sizeModel.setObservation(request.getParameter("txtobservation"));

                if (size.Edit(sizeModel)) {
                    codeStatus = "Success";
                    message = "Se ha guardado correctamente";
                } else {
                    codeStatus = "Error";
                    message = "Talla ya existe";
                }

            }
            break;
        }

        sizeList = size.List();
        request.setAttribute("sizeList", sizeList);
        request.setAttribute("codeStatus", codeStatus);
        request.setAttribute("message", message);
        request.getRequestDispatcher("com.cidenet.kardex.view/size.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
