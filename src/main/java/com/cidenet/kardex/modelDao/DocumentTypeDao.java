/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.modelDao;

import com.cidenet.kardex.configuration.Conexion;
import com.cidenet.kardex.model.DocumentModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author jcuesta
 */
public class DocumentTypeDao {

    String SQL = "";
    Conexion conn = Conexion.getInstance();

    public List AssetList() {
        List<DocumentModel> documentList = new ArrayList<>();
        try {
            SQL = "SELECT * FROM TiposDocumentos WHERE  estado = ?";

            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, "activo");
            conn.resultado = conn.sentencia.executeQuery();
            while (conn.resultado.next()) {
                DocumentModel document = new DocumentModel();
                document.setDocumentTypeId(conn.resultado.getInt(1));
                document.setDescription(conn.resultado.getString(2));

                documentList.add(document);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar existencia de tipos de documentos \n" + e);
        }
        return documentList;
    }

}
