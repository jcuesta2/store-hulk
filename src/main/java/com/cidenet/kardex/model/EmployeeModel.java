/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.model;

import java.util.Objects;

/**
 *
 * @author jcuesta
 */
public class EmployeeModel {

    private int employeeId;
    private String documentType;
    private String documentNumber;
    private String name;
    private String lastname;
    private String contact;
    private String status;

    @Override
    public String toString() {
        return "Employee{" + "employeeId=" + employeeId + ", documentType=" + documentType + ", documentNumber=" + documentNumber + ", name=" + name + ", lastname=" + lastname + ", contact=" + contact + ", status=" + status + '}';
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public EmployeeModel() {
    }

    public EmployeeModel(int employeeId, String documentType, String documentNumber, String name, String lastname, String contact, String status) {
        this.employeeId = employeeId;
        this.documentType = documentType;
        this.documentNumber = documentNumber;
        this.name = name;
        this.lastname = lastname;
        this.contact = contact;
        this.status = status;
    }

    public EmployeeModel(String documentType, String documentNumber, String name, String lastname, String contact, String status) {

        this.documentType = documentType;
        this.documentNumber = documentNumber;
        this.name = name;
        this.lastname = lastname;
        this.contact = contact;
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash *= (hash + this.employeeId);
        hash *= (hash + Objects.hashCode(this.documentType));
        hash *= (hash + Objects.hashCode(this.documentNumber));
        hash *= (hash + Objects.hashCode(this.name));
        hash *= (hash + Objects.hashCode(this.lastname));
        hash *= (hash + Objects.hashCode(this.contact));
        hash *= (hash + Objects.hashCode(this.status));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EmployeeModel other = (EmployeeModel) obj;
        if (this.employeeId != other.employeeId) {
            return false;
        }
        if (!Objects.equals(this.documentType, other.documentType)) {
            return false;
        }
        if (!Objects.equals(this.documentNumber, other.documentNumber)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.lastname, other.lastname)) {
            return false;
        }
        if (!Objects.equals(this.contact, other.contact)) {
            return false;
        }
        if (!Objects.equals(this.status, other.status)) {
            return false;
        }
        return true;
    }

}
