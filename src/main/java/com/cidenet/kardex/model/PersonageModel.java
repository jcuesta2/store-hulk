/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.model;

/**
 *
 * @author jcuesta
 */
public class PersonageModel {
    private int personageId;
    private String supplier;
    private String name;
    private String status;

    @Override
    public String toString() {
        return "PersonageModel{" + "personageId=" + personageId + ", supplier=" + supplier + ", name=" + name + ", status=" + status + '}';
    }
    

    public PersonageModel() {
    }

    public PersonageModel(int personageId, String supplier, String name, String status) {
        this.personageId = personageId;
        this.supplier = supplier;
        this.name = name;
        this.status = status;
    }

    public int getPersonageId() {
        return personageId;
    }

    public void setPersonageId(int personageId) {
        this.personageId = personageId;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
