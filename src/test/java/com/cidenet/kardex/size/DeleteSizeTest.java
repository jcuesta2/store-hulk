/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.size;

import com.cidenet.kardex.modelDao.SizeDao;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author jcuesta
 */
public class DeleteSizeTest {

    int sizeId = 8;
    String status = "eliminado";

    @Test
    public void DeletedSizeTest() {
        SizeDao sizeDao = new SizeDao();
        
        boolean result = sizeDao.ChangeStatus(sizeId, status);
        assertTrue(result);
        
        if (result) {
            status = "activo";
            sizeDao.ChangeStatus(sizeId, status);
        }
    }
}
