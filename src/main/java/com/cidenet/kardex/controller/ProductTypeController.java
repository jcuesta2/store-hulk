/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.controller;

import com.cidenet.kardex.model.ProductTypeModel;
import com.cidenet.kardex.modelDao.ProductTypeDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

/**
 *
 * @author jcuesta
 */
@WebServlet(name = "ProductTypeController", urlPatterns = {"/ProductTypeController"})
public class ProductTypeController extends HttpServlet {

    ProductTypeModel productTypeModel = new ProductTypeModel();
    List<ProductTypeModel> productTypeList, EditModal = new ArrayList<>();
    ProductTypeDao product = new ProductTypeDao();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ProductTypeController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ProductTypeController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String codeStatus = "";
        String message = "";
        String productToSearch = "";
        String openModal = "";
        int productId;
        String action = request.getParameter("action");
        switch (action) {
            case "ProductTypeList": {

            }
            break;
            case "SearchProduct": {
                productToSearch = request.getParameter("txtsearchValue");

            }
            break;
            case "StatusChange": {
                int resp = JOptionPane.showConfirmDialog(null, "¿Está seguro que desea eliminar el producto? ", "Alerta!", JOptionPane.YES_NO_OPTION);
                if (resp == 0) {

                    productId = Integer.parseInt(request.getParameter("productId"));
                    productTypeModel.setProductTypeId(productId);
                    productTypeModel.setStatus("eliminado");
                    if (product.ChangeStatus(productTypeModel)) {
                        codeStatus = "Success";
                        message = "Producto eliminado correctamente";
                    } else {
                        codeStatus = "Error";
                        message = "No se pudo cambiar el estado del empleado";
                    }

                } else {
                    codeStatus = "Información";
                    message = "No se realizaron cambios";
                }
            }
            break;
            case "EditModal": {
                productId = Integer.parseInt(request.getParameter("productId"));
                for (ProductTypeModel productItem : productTypeList) {
                    EditModal.clear();
                    if (productId == productItem.getProductTypeId()) {
                        openModal = "Edit";
                        EditModal.add(productItem);
                        request.setAttribute("EditModal", EditModal);
                        break;
                    }
                }

            }
            break;
        }
        if (action.equals("SearchProduct")) {
            productTypeList = product.Search(productToSearch);
        } else {
            productTypeList = product.List();
        }
        request.setAttribute("codeStatus", codeStatus);
        request.setAttribute("message", message);
        request.setAttribute("openModal", openModal);
        request.setAttribute("productTypeList", productTypeList);
        request.getRequestDispatcher("com.cidenet.kardex.view/productType.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String codeStatus = "";
        String message = "";
        String action = request.getParameter("action");

        switch (action) {
            case "ProducTypeAdd": {
                productTypeModel.setDescription(request.getParameter("txtproduct"));
                productTypeModel.setStatus(request.getParameter("txtstatus"));

                if (product.Add(productTypeModel)) {
                    codeStatus = "Success";
                    message = "Se ha guardado correctamente";
                } else {
                    codeStatus = "Error";
                    message = "Producto ya existe";
                }
                request.setAttribute("codeStatus", codeStatus);
                request.setAttribute("message", message);
                String table = "Product";
                request.setAttribute("table", table);
            }
            break;
            case "ProducTypeEdit": {
                productTypeModel.setDescription(request.getParameter("txtproduct"));
                productTypeModel.setStatus(request.getParameter("txtstatus"));
                productTypeModel.setProductTypeId(Integer.parseInt(request.getParameter("txtproductId")));
                if (product.Edit(productTypeModel)) {
                    codeStatus = "Success";
                    message = "Se ha guardado correctamente";
                } else {
                    codeStatus = "Error";
                    message = "Producto ya existe";
                }
                request.setAttribute("codeStatus", codeStatus);
                request.setAttribute("message", message);
                String table = "Product";
                request.setAttribute("table", table);
            }
            break;
        }
        productTypeList = product.List();
        request.setAttribute("productTypeList", productTypeList);
        request.getRequestDispatcher("com.cidenet.kardex.view/productType.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
