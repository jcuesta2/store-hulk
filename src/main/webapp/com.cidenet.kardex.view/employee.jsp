<%-- 
    Document   : empleado
    Created on : Nov 15, 2019, 6:08:15 PM
    Author     : jcuesta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        <style>

            .table-fixed tbody {
                height: 300px;
                overflow-y: auto;
                width: 100%;
            }

            .table-fixed thead,
            .table-fixed tbody,
            .table-fixed tr,
            .table-fixed td,
            .table-fixed th {
                display: block;
            }

            .table-fixed tbody td,
            .table-fixed tbody th,
            .table-fixed thead > tr > th {
                float: left;
                position: relative;

                &::after {
                    content: '';
                    clear: both;
                    display: block;
                }
            }



            body {
                background: #74ebd5;
                background: -webkit-linear-gradient(to right, #74ebd5, #ACB6E5);
                background: linear-gradient(to right, #74ebd5, #ACB6E5);
                min-height: 100vh;

            }


        </style>

    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">
                <img src="/docs/4.3/assets/brand/bootstrap-solid.svg" width="30" height="30" alt="">
                Tienda-COMICS
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="../index.jsp">Inicio <span class="sr-only">(current)</span></a>
                    </li>                    
                    <li class="nav-item">
                        <a class="nav-link" href="StoreController?action=StoreList">Tienda</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="SalesController?action=SaleList">Ventas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="StorageController?action=StorageList">Bodega</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="OrderController?action=OrderList">Pedidos a proveedores</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Configuraciones
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">                            
                            <a class="dropdown-item" href="PersonageController?action=PersonageList">Personajes</a>
                            <a class="dropdown-item" href="SupplierController?action=SupplierList" title="Clic para listar">Proveedores</a>
                            <a class="dropdown-item" href="ProductTypeController?action=ProductTypeList">Tipos de productos</a>
                            <a class="dropdown-item" href="SizeController?action=SizeList">Tallas o tamaños</a>
                            <a class="dropdown-item" href="PriceOfProductController?action=PriceList">Cotización final de productos</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <div>
            <nav class="navbar navbar-dark" >

                <div class="row">
                    <div class="col-md-3 col-md-offset-9 text-right" >
                        <div class="btn-group" role="group">                            
                            <a class="btn btn-primary my-2 my-sm-0" href="EmployeeController?action=ListActiveDocument" title="Click para agregar">Nuevo</a>
                        </div>
                    </div>
                </div>

                <form action="EmployeeController" method="GET" class="form-inline">
                    <label class=" my-2 my-sm-0" style="color:  red">*</label>&nbsp;<span class="glyphicon glyphicon-step-forward"></span>
                    <input name="txtsearchValue" class="form-control mr-sm-2" type="search" placeholder="Numero de documento..."  maxlength="12"  title="Buscar empleado" required> 

                    <button name="action" class="btn btn-primary my-2 my-sm-0" type="submit" value="SearchEmployee"   title="Click para buscar" >Buscar</button>&nbsp;<span class="glyphicon glyphicon-step-forward"></span>
                    <a class="btn btn-primary my-2 my-sm-0" role="button" href="EmployeeController?action=EmployeeList" data-toggle="tooltip" data-placement="right" title="Click para listar">
                        Ver Todos
                    </a>&nbsp;<span class="glyphicon glyphicon-step-forward"></span>&nbsp;<span class="glyphicon glyphicon-step-forward"></span>

                </form>

            </nav>
            <h4 align=center>Empleados</h4>
            <div class="container py-5">
                <div class="row">
                    <div class="col-md-12  mx-auto bg-white  rounded shadow"> <!-- Aqui esta el detalle-->                                        
                        <!-- Fixed header table-->
                        <div class="table-responsive">
                            <table class="table table-fixed">
                                <thead>
                                    <tr>                    
                                        <th scope="col" class="col-2">Documento</th>
                                        <th scope="col" class="col-2">Numero</th>
                                        <th scope="col" class="col-2">Nombre Completo</th>                                        
                                        <th scope="col" class="col-2">Contacto</th>
                                        <th scope="col" class="col-2">Estado</th>
                                        <th scope="col" class="col-2">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test="${employeeList != null && not empty employeeList}">
                                        <c:forEach var="employee" items="${employeeList}">
                                            <tr>
                                                <td style="display: none;" scope="row">${employee.employeeId}</td>   
                                                <td class="col-2">${employee.documentType}</td> 
                                                <td class="col-2">${employee.documentNumber}</td>
                                                <td class="col-2">${employee.name} ${employee.lastname}</td>                                                 
                                                <td class="col-2">${employee.contact}</td> 
                                                <td class="col-2">${employee.status}</td> 
                                                <td class="col-2">        
                                                    <a id="btnEditarTar" class="btn btn-primary" role="button" href="EmployeeController?action=EditModal&employeeId=${employee.employeeId}"  data-placement="right" title="Click para Editar">Editar</a>                                    
                                                    <a class=" btn btn-danger" role="button" href="EmployeeController?action=StatusChange&employeeId=${employee.employeeId}" data-toggle="tooltip" data-placement="right" title="Click para cambiar estado">Eliminar</a>                                
                                                </td> 
                                            </tr>
                                        </c:forEach>
                                    </c:if>                        
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal Crear empleado -->    
            <c:if test="${openModal == 'Add'}">
                <div class="modal fade" id="employeeModal" tabindex="-1" role="dialog" aria-labelledby="employeeModal" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div id="divModalHeadCrear" class="modal-header">
                                <h5 class="modal-title" id="ModalPedido">Editar Empleado</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form name="Modalemployee" id="employeeModalForm" action="EmployeeController" method="POST">
                                    <div class="form-group">                                   
                                        <label style="color: red" for="message-text" class="col-form-label">* <label style="color: black" for="message-text" class="col-form-label">Tipo de documento :</label> </label>
                                        <select class="custom-select" id="txttypeDocument" name="txttypeDocument"  required>   
                                            <option value="" selected>Selecciona el tipo de documento....</option>
                                            <c:forEach var="document" items="${documentList}">
                                                <option value="${document.documentTypeId}">${document.description}</option>
                                            </c:forEach>                                             
                                        </select>
                                    </div>                           
                                    <div class="form-group">
                                        <label style="color: red" for="message-text" class="col-form-label">* <label style="color: black">Numero de documento</label> </label>
                                        <input type="number" class="form-control"  id="txtdocumentNumber" name="txtdocumentNumber" maxlength="12"  title="Numero de documento" required>      
                                    </div>
                                    <div class="form-group">
                                        <label style="color: red" for="recipient-name" class="col-form-label">*  <label style="color: black" for="recipient-name" class="col-form-label">Nombre :</label></label>
                                        <input type="text" class="form-control"  id="txtname" name="txtname"  title="Nombre de empleado" required>      
                                    </div>
                                    <div class="form-group">
                                        <label style="color: red" for="recipient-name" class="col-form-label">* <label style="color: black" for="recipient-name" class="col-form-label">Apellido :</label></label>
                                        <input type="text" class="form-control"  id="txtsurname" name="txtsurname"  title="Apellido" required>      
                                    </div>
                                    <div class="form-group">
                                        <label style="color: red" for="recipient-name" class="col-form-label">*  <label style="color: black" for="recipient-name" class="col-form-label">Contacto :</label></label>
                                        <input type="text" class="form-control"  id="txtsurname" name="txtcontact"  title="Contacto" required>      
                                    </div>
                                    <div class="form-group">                                   
                                        <label style="color: red" for="message-text" class="col-form-label">*  <label style="color: black" for="message-text" class="col-form-label">Estado :</label></label>
                                        <select class="custom-select" id="txtstatus" name="txtstatus"  required>                                       
                                            <option value="" selected>Selecciona el estado....</option>
                                            <option value="activo">Activo</option>
                                            <option value="inactivo">Inactivo</option> 
                                        </select>
                                    </div>  
                                    <div class="modal-footer">
                                        <button id="close" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                        <button id="btn-guardar" type="submit"  class="btn btn-info" name="action" value="EmployeeAdd">Guardar</button>
                                    </div>
                                </form>   
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>
            <!-- Modal Editar empleado -->    
            <c:if test="${openModal == 'Edit'}">
                <div class="modal fade" id="employeeModal" tabindex="-1" role="dialog" aria-labelledby="employeeModal" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div id="divModalHeadCrear" class="modal-header">
                                <h5 class="modal-title" id="ModalPedido">Registrar Empleado</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form name="Modalemployee" id="employeeModalForm" action="EmployeeController" method="POST">
                                    <div class="form-group">                                   
                                        <label style="color: red" for="message-text" class="col-form-label">* <label style="color: black" for="message-text" class="col-form-label">Tipo de documento :</label> </label>
                                        <select class="custom-select" id="txttypeDocument" name="txttypeDocument"  required>                                      
                                            <c:forEach var="document" items="${documentList}">
                                                <option value="${document.documentTypeId}">${document.description}</option>
                                            </c:forEach>                                             
                                        </select>
                                    </div>
                                    <c:forEach var="employee" items="${EditModal}">
                                        <input id="txtemployeeId" name="txtemployeeId" type="hidden" value="${employee.employeeId}">
                                        <div class="form-group">
                                            <label style="color: red" for="message-text" class="col-form-label">* <label style="color: black">Numero de documento</label> </label>
                                            <input type="number" class="form-control"  id="txtdocumentNumber" name="txtdocumentNumber" maxlength="12"  title="Numero de documento" value="${employee.documentNumber}" required>      
                                        </div>
                                        <div class="form-group">
                                            <label style="color: red" for="recipient-name" class="col-form-label">*  <label style="color: black" for="recipient-name" class="col-form-label">Nombre :</label></label>
                                            <input type="text" class="form-control" id="txtname" name="txtname"  title="Nombre de empleado" value="${employee.name}" required>      
                                        </div>
                                        <div class="form-group">
                                            <label style="color: red" for="recipient-name" class="col-form-label">* <label style="color: black" for="recipient-name" class="col-form-label">Apellido :</label></label>
                                            <input type="text" class="form-control"  id="txtsurname" name="txtsurname"  title="Apellido" value="${employee.lastname}" required>      
                                        </div>
                                        <div class="form-group">
                                            <label style="color: red" for="recipient-name" class="col-form-label">*  <label style="color: black" for="recipient-name" class="col-form-label">Contacto :</label></label>
                                            <input type="text" class="form-control"  id="txtsurname" name="txtcontact"  title="Contacto" value="${employee.contact}" required>      
                                        </div>
                                    </c:forEach>
                                    <div class="form-group">                                   
                                        <label style="color: red" for="message-text" class="col-form-label">*  <label style="color: black" for="message-text" class="col-form-label">Estado :</label></label>
                                        <select class="custom-select" id="txtstatus" name="txtstatus"  required>                                       
                                            <option value="" selected>Selecciona el estado....</option>
                                            <option value="activo">Activo</option>
                                            <option value="inactivo">Inactivo</option> 
                                        </select>
                                    </div>
                                    <div class="modal-footer">
                                        <button id="close" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                        <button id="btn-guardar" type="submit"  class="btn btn-info" name="action" value="EmployeeEdit">Guardar</button>
                                    </div>
                                </form>   
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>
            <!-- Modal Crear Response -->  
            <c:if test="${not empty codeStatus }">
                <div class="container">
                    <div class="modal fade" id="ModalResponse" role="dialog">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content"> 
                                <div id="divModalHeadConf"  class="modal-header">
                                    <h4 class="modal-title">${codeStatus}</h4>                        
                                </div>
                                <div style="alignment-baseline: central" class="modal-body">
                                    <p>${message}</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>

            <script>
                $("#ModalResponse").modal("show");
                $("#employeeModal").modal("show");

            </script>
    </body>
</html>
