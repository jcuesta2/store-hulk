/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.controller;

import com.cidenet.kardex.model.PersonageModel;
import com.cidenet.kardex.model.SupplierModel;
import com.cidenet.kardex.modelDao.PersonageDao;
import com.cidenet.kardex.modelDao.SupplierDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

/**
 *
 * @author jcuesta
 */
@WebServlet(name = "PersonageController", urlPatterns = {"/PersonageController"})
public class PersonageController extends HttpServlet {

    List<PersonageModel> personageList, EditModal = new ArrayList<>();
    List<SupplierModel> listActiveSupplier = new ArrayList<>();

    PersonageModel personageModel = new PersonageModel();

    SupplierDao supplier = new SupplierDao();
    PersonageDao personage = new PersonageDao();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PersonageController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PersonageController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int personageId;
        String codeStatus = "";
        String message = "";
        String personageToSearch = "";
        String openModal = "";
        String action = request.getParameter("action");

        switch (action) {
            case "PersonageList": {
            }
            break;

            case "SearchPersonage": {
                personageToSearch = request.getParameter("txtsearchValue");

            }
            break;
            case "ListActiveSupplier": {
                openModal = "Add";
                listActiveSupplier = supplier.AssetList();
                request.setAttribute("listActiveSupplier", listActiveSupplier);

            }
            break;
            case "EditModal": {
                personageId = Integer.parseInt(request.getParameter("personageId"));
                for (PersonageModel personageItem : personageList) {
                    EditModal.clear();
                    if (personageId == personageItem.getPersonageId()) {
                        openModal = "Edit";
                        EditModal.add(personageItem);
                        listActiveSupplier = supplier.AssetList();
                        request.setAttribute("EditModal", EditModal);
                        request.setAttribute("listActiveSupplier", listActiveSupplier);
                        break;
                    }
                }

            }
            break;
            case "StatusChange": {
                int resp = JOptionPane.showConfirmDialog(null, "¿Está seguro que desea eliminar el personaje? ", "Alerta!", JOptionPane.YES_NO_OPTION);
                if (resp == 0) {

                    personageId = Integer.parseInt(request.getParameter("personageId"));

                    personageModel.setPersonageId(personageId);
                    personageModel.setStatus("Eliminado");
                    if (personage.ChangeStatus(personageModel)) {
                        codeStatus = "Success";
                        message = "Estado actualizado correctamente";
                    } else {
                        codeStatus = "Error";
                        message = "No se pudo cambiar el estado del empleado";
                    }

                } else {
                    codeStatus = "Información";
                    message = "No se realizaron cambios";
                }

            }
            break;

        }
        if (action.equals("SearchPersonage")) {
            personageList = personage.Search(personageToSearch);
        } else {
            personageList = personage.List();
        }
        request.setAttribute("personageList", personageList);
        request.setAttribute("codeStatus", codeStatus);
        request.setAttribute("message", message);
        request.setAttribute("openModal", openModal);
        request.getRequestDispatcher("com.cidenet.kardex.view/personage.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String codeStatus = "";
        String message = "";
        String action = request.getParameter("action");

        switch (action) {
            case "PersonageAdd": {
                personageModel.setSupplier(request.getParameter("txtsuppliers"));
                personageModel.setName(request.getParameter("txtpersonage"));
                personageModel.setStatus(request.getParameter("txtstatus"));

                if (personage.Add(personageModel)) {
                    codeStatus = "Success";
                    message = "Se ha guardado correctamente";
                } else {
                    codeStatus = "Error";
                    message = "Personaje ya existe";
                }
                request.setAttribute("codeStatus", codeStatus);
                request.setAttribute("message", message);
                String table = "Personage";
                request.setAttribute("table", table);
            }
            break;
            case "PersonageEdit": {
                personageModel.setPersonageId(Integer.parseInt(request.getParameter("txtpersonageId")));
                personageModel.setSupplier(request.getParameter("txtsuppliers"));
                personageModel.setName(request.getParameter("txtpersonage"));
                personageModel.setStatus(request.getParameter("txtstatus"));

                if (personage.Edit(personageModel)) {
                    codeStatus = "Success";
                    message = "Se ha guardado correctamente";
                } else {
                    codeStatus = "Error";
                    message = "Proveedor ya existe";
                }

            }
            break;
        }

        personageList = personage.List();
        request.setAttribute("personageList", personageList);
        request.setAttribute("codeStatus", codeStatus);
        request.setAttribute("message", message);
        request.getRequestDispatcher("com.cidenet.kardex.view/personage.jsp").forward(request, response);

    }
}
