/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.modelDao;

import com.cidenet.kardex.configuration.Conexion;
import com.cidenet.kardex.interfaces.General;
import com.cidenet.kardex.interfaces.Sales;
import com.cidenet.kardex.model.OrderModel;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author jcuesta
 */
public class SalesDao implements Sales, General {

    String SQL = "";
    Conexion conn = Conexion.getInstance();

    @Override
    public void Add(OrderModel sale) {
        Date date = new Date();
        String dateSale = new SimpleDateFormat("dd/MM/yyyy").format(date);
        try {
            SQL = "INSERT INTO Ventas (descripcion, talla, cantidad, valor_unidad, total_pago, fecha)"
                    + "values (?, ?, ?, ?, ?, ?)";
            conn.sentencia = conn.conexion.prepareStatement(SQL);            
            conn.sentencia.setString(1, sale.getProductType());
            conn.sentencia.setString(2, sale.getSize());
            conn.sentencia.setInt(3, sale.getQuantity());
            conn.sentencia.setInt(4, sale.getUnityValue());
            conn.sentencia.setInt(5, sale.getQuantity() * sale.getUnityValue());
            conn.sentencia.setString(6, dateSale);
            conn.sentencia.executeUpdate();
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al agregar Producto a la venta \n" + e);
        }
    }

    @Override
    public List List() {
        
        List<OrderModel> saleList = new ArrayList<>();

        try {
            SQL = "SELECT * FROM Ventas";

            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.resultado = conn.sentencia.executeQuery();

            while (conn.resultado.next()) {
                OrderModel sale = new OrderModel();
                sale.setStore(String.valueOf(conn.resultado.getInt(1)));
                sale.setProductType(conn.resultado.getString(2));                
                sale.setSize(conn.resultado.getString(3));
                sale.setQuantity(conn.resultado.getInt(4));
                sale.setUnityValue(conn.resultado.getInt(5));
                sale.setTotalValue(conn.resultado.getInt(6));
                sale.setApplicationDate(conn.resultado.getString(7));
                saleList.add(sale);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al listar productos de la tienda \n" + e);
        }
        return saleList;
        
    }

    @Override
    public java.util.List Search(String description) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
