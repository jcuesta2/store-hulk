/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.size;

import com.cidenet.kardex.model.SizeModel;
import com.cidenet.kardex.modelDao.SizeDao;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author jcuesta
 */
@RunWith(Parameterized.class)
public class SearchSizeTest {

    private final String description;
    private List<SizeModel> list = new ArrayList<>();

    public SearchSizeTest(String description, List<SizeModel> list) {
        this.description = description;
        this.list = list;
    }

    @Parameterized.Parameters
    public static Collection data() {
        List<SizeModel> list = new ArrayList<>();

        list.add(new SizeModel(6, "Xs", "Normal", "Activo"));

        return Arrays.asList(new Object[][]{
            {"xs", list}
        });
    }

    @Test
    public void testSearch() {
        try {
            SizeDao sizeDao = new SizeDao();
            List<SizeModel> result = new ArrayList<>();

            result = sizeDao.Search(description);

            assertThat(result, is(list));
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

}
