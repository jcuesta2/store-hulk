<%-- 
    Document   : sale
    Created on : Dec 19, 2019, 5:41:16 PM
    Author     : jcuesta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <style>

            .table-fixed tbody {
                height: 300px;
                overflow-y: auto;
                width: 100%;
            }

            .table-fixed thead,
            .table-fixed tbody,
            .table-fixed tr,
            .table-fixed td,
            .table-fixed th {
                display: block;
            }

            .table-fixed tbody td,
            .table-fixed tbody th,
            .table-fixed thead > tr > th {
                float: left;
                position: relative;

                &::after {
                    content: '';
                    clear: both;
                    display: block;
                }
            }



            body {
                background: #74ebd5;
                background: -webkit-linear-gradient(to right, #74ebd5, #ACB6E5);
                background: linear-gradient(to right, #74ebd5, #ACB6E5);
                min-height: 100vh;

            }


        </style>

    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">
                <img src="/docs/4.3/assets/brand/bootstrap-solid.svg" width="30" height="30" alt="">
                Tienda-COMICS
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Inicio <span class="sr-only">(current)</span></a>
                    </li>                     
                    <li class="nav-item">
                        <a class="nav-link" href="StoreController?action=StoreList">Tienda</a>
                    </li>                   
                    <li class="nav-item">
                        <a class="nav-link" href="StorageController?action=StorageList">Bodega</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="OrderController?action=OrderList">Pedidos a proveedores</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Configuraciones
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="EmployeeController?action=EmployeeList">Empleados</a>
                            <a class="dropdown-item" href="PersonageController?action=PersonageList">Personajes</a>
                            <a class="dropdown-item" href="SupplierController?action=SupplierList" title="Clic para listar">Proveedores</a>                           
                            <a class="dropdown-item" href="ProductTypeController?action=ProductTypeList">Tipos de productos</a>
                            <a class="dropdown-item" href="SizeController?action=SizeList">Tallas o tamaños</a>
                            <a class="dropdown-item" href="PriceOfProductController?action=PriceList">Cotización final de productos</a>                            
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <h4 align=center>Ventas Realizadas</h4>
        <div class="container py-5">
            <div class="row">
                <div class="col-md-12  mx-auto bg-white  rounded shadow"> <!-- Aqui esta el detalle-->                                        
                    <!-- Fixed header table-->
                    <div class="table-responsive">
                        <table class="table table-fixed">                            
                            <thead>                                
                            <tr>                    
                                <th scope="col" class="col-1">N°venta</th>
                                <th scope="col" class="col-2">Descripción</th> 
                                <th scope="col" class="col-2">Talla</th>
                                <th scope="col" class="col-1">Cantidad</th>
                                <th scope="col" class="col-2">Valor Unidad</th>
                                <th scope="col" class="col-2">Valor Total</th>
                                <th scope="col" class="col-2">Fecha</th>
                            </tr>
                            </thead>
                            <tbody>
                                <c:if test="${saleList != null && not empty saleList}">
                                    <c:forEach var="sale" items="${saleList}">
                                        <tr>                                 
                                            <td class="col-1">${sale.store}</td> 
                                            <td class="col-2">${sale.productType}</td>
                                            <td class="col-2">${sale.size}</td> 
                                            <td class="col-1">${sale.quantity}</td>
                                            <td class="col-2">${sale.unityValue}</td>
                                            <td class="col-2">${sale.totalValue}</td>
                                            <td class="col-2">${sale.applicationDate}</td>                                
                                        </tr>
                                    </c:forEach>                            
                                </c:if>                        
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
