/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.controller;

import com.cidenet.kardex.model.SupplierModel;
import com.cidenet.kardex.modelDao.SupplierDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

/**
 *
 * @author jcuesta
 */
@WebServlet(name = "SupplierController", urlPatterns = {"/SupplierController"})
public class SupplierController extends HttpServlet {

    List<SupplierModel> supplierList, EditModal = new ArrayList<>();

    SupplierModel supplierModel = new SupplierModel();
    SupplierDao supplier = new SupplierDao();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SupplierController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SupplierController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int supplierId;
        String supplierToSearch = "";
        String codeStatus = "";
        String message = "";
        String openModal = "";

        String action = request.getParameter("action");
        switch (action) {
            case "SupplierList": {

            }
            break;

            case "SearchSupplier": {
                supplierToSearch = request.getParameter("txtsearchValue");

            }
            break;
            case "EditModal": {
                supplierId = Integer.parseInt(request.getParameter("supplierId"));
                for (SupplierModel supplierItem : supplierList) {
                    EditModal.clear();
                    if (supplierId == supplierItem.getEditorialId()) {
                        openModal = "Edit";
                        EditModal.add(supplierItem);
                        request.setAttribute("EditModal", EditModal);
                        break;
                    }
                }

            }
            break;
            case "StatusChange": {
                int resp = JOptionPane.showConfirmDialog(null, "¿Está seguro que desea eliminar la talla? ", "Alerta!", JOptionPane.YES_NO_OPTION);
                if (resp == 0) {

                    supplierId = Integer.parseInt(request.getParameter("supplierId"));
                    supplierModel.setEditorialId(supplierId);
                    supplierModel.setStatus("Eliminado");
                    if (supplier.ChangeStatus(supplierModel)) {
                        codeStatus = "Success";
                        message = "Proveedor eliminado correctamente";
                    } else {
                        codeStatus = "Error";
                        message = "No se pudo eliminar Proveedor";
                    }

                } else {
                    codeStatus = "Información";
                    message = "No se realizaron cambios";
                }
            }
            break;
        }
        if (action.equals("SearchSupplier")) {
            supplierList = supplier.Search(supplierToSearch);
        } else {
            supplierList = supplier.List();
        }
        request.setAttribute("codeStatus", codeStatus);
        request.setAttribute("message", message);
        request.setAttribute("openModal", openModal);
        request.setAttribute("supplierList", supplierList);
        request.getRequestDispatcher("com.cidenet.kardex.view/supplier.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String codeStatus = "";
        String message = "";
        String action = request.getParameter("action");

        switch (action) {
            case "SupplierAdd": {
                supplierModel.setDescription(request.getParameter("txtsupplier"));
                supplierModel.setStatus(request.getParameter("txtstatus"));

                if (supplier.Add(supplierModel)) {
                    codeStatus = "Success";
                    message = "Se ha guardado correctamente";
                } else {
                    codeStatus = "Error";
                    message = "Proveedor ya existe";
                }
                request.setAttribute("codeStatus", codeStatus);
                request.setAttribute("message", message);
                String table = "Supplier";
                request.setAttribute("table", table);
            }
            break;
            case "SupplierEdit": {
                supplierModel.setEditorialId(Integer.parseInt(request.getParameter("txtsupplierId")));
                supplierModel.setDescription(request.getParameter("txtsupplier"));
                supplierModel.setStatus(request.getParameter("txtstatus"));

                if (supplier.Edit(supplierModel)) {
                    codeStatus = "Success";
                    message = "Se ha guardado correctamente";
                } else {
                    codeStatus = "Error";
                    message = "Proveedor ya existe";
                }

            }
            break;

        }
        supplierList = supplier.List();        
        request.setAttribute("codeStatus", codeStatus);
        request.setAttribute("message", message);
        request.setAttribute("supplierList", supplierList);
        request.getRequestDispatcher("com.cidenet.kardex.view/supplier.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
