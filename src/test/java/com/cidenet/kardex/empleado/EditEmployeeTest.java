/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.empleado;

import com.cidenet.kardex.model.EmployeeModel;
import com.cidenet.kardex.modelDao.EmployeeDao;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author jcuesta
 */
@RunWith(Parameterized.class)
public class EditEmployeeTest {

    EmployeeModel employee = new EmployeeModel();

    public EditEmployeeTest(EmployeeModel employee) {
        this.employee = employee;
    }

    @Parameterized.Parameters
    public static Collection data() {

        EmployeeModel employee = new EmployeeModel(1, "1", "1003969900", "stefano", "Moreno", "3124460030", "Activo");

        return Arrays.asList(new Object[]{
            employee
        });
    }

    @Test
    public void EditEmployeeTest() {
        try {
            EmployeeDao empDao = new EmployeeDao();

            boolean resultado = empDao.Edit(employee);
            assertTrue(resultado);

        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
}
