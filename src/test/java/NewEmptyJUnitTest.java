/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cidenet.kardex.model.EmployeeModel;
import com.cidenet.kardex.modelDao.EmployeeDao;
import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.CoreMatchers.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jcuesta
 */
public class NewEmptyJUnitTest {
    
    

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void testBuscar() {
         List<EmployeeModel> listaEsperada = new ArrayList<>();
         List<EmployeeModel> resultado = new ArrayList<>();
         
         EmployeeDao empDao = new EmployeeDao();
         EmployeeModel employee = new EmployeeModel();
         
         resultado = empDao.Search("110");
         
         employee.setEmployeeId(4);
         employee.setDocumentType("pasaporte");
         employee.setDocumentNumber("110053563");
         employee.setName("lucia");
         employee.setLastname("perez");
         employee.setContact("3124465230");
         employee.setStatus("Activo");
         
         listaEsperada.add(employee);         
         assertThat(resultado, is(listaEsperada));
     }

   
}
