/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.empleado;

import com.cidenet.kardex.configuration.Conexion;
import com.cidenet.kardex.model.EmployeeModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author jcuesta
 */
public class TestEmployeeDao {

    String SQL = "";
    Conexion conn = Conexion.getInstance();

    public void RemoveEmployeeTest(String documentNumber) {
        try {
            SQL = "DELETE FROM Empleados WHERE numero_documento = ?";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, documentNumber);
            conn.sentencia.executeUpdate();
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al eliminar empleado creado en la prueba \n" + e);
        }
    }

    public List ListEmployeeToEditTest(int employeeId) {
        List<EmployeeModel> employeeList = new ArrayList<>();
        try {
            SQL = "SELECT e.id_empleado, e.numero_documento, initcap(e.nombre), initcap(e.apellido), initcap(e.contacto), initcap(e.estado), initcap(td.descripcion) "
                    + " FROM Empleados e  join TiposDocumentos td on e.tipo_documento = td.id_tipo_documento "
                    + "WHERE e.id_empleado = ? ";

            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setInt(1, employeeId);
            conn.resultado = conn.sentencia.executeQuery();

            if (conn.resultado.next()) {
                EmployeeModel employee = new EmployeeModel();
                employee.setEmployeeId(conn.resultado.getInt(1));
                employee.setDocumentNumber(conn.resultado.getString(2));
                employee.setName(conn.resultado.getString(3));
                employee.setLastname(conn.resultado.getString(4));
                employee.setContact(conn.resultado.getString(5));
                employee.setStatus(conn.resultado.getString(6));
                employee.setDocumentType(conn.resultado.getString(7));

                employeeList.add(employee);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al consultar existencia de empleado \n" + e);
        }
        return employeeList;
    }
}
