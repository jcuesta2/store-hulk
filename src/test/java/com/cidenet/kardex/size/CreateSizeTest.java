/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.size;

import com.cidenet.kardex.model.SizeModel;
import com.cidenet.kardex.modelDao.SizeDao;
import java.util.Arrays;
import java.util.Collection;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


/**
 *
 * @author jcuesta
 */
@RunWith(Parameterized.class)
public class CreateSizeTest {
    
    SizeModel size = new SizeModel();
    
    public CreateSizeTest(SizeModel size){
        this.size = size;
    }
    
    @Parameterized.Parameters
    public static Collection data() {

         SizeModel size = new SizeModel("xxl", "activo", "normal");

        return Arrays.asList(new Object[]{
            size
        });
    }
    
    @Test
    public void CreateSize(){
        try {
            SizeDao sizeDao = new SizeDao();
             TestSizeDao sizeDaoTest = new TestSizeDao();
                     
            boolean result = sizeDao.Add(size);
            assertTrue(result); 
         if (result) {
                sizeDaoTest.RemoveSizeTest(size.getDescription());
            }
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
}
