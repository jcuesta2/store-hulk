/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.interfaces;

import com.cidenet.kardex.model.OrderModel;
import java.util.List;

/**
 *
 * @author jcuesta
 */
public interface Storage {

    public boolean ValidateStorageExistence(List<OrderModel> order);

    public boolean Add(List<OrderModel> order);
    
    public List UpdateStorageStock(OrderModel store);
    
    
   
}
