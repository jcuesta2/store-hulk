/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.empleado;

import com.cidenet.kardex.model.EmployeeModel;
import com.cidenet.kardex.modelDao.EmployeeDao;
import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author jcuesta
 */
@RunWith(Parameterized.class)
public class CreateEmployeeTest {
    
    EmployeeModel employee = new EmployeeModel();

    public CreateEmployeeTest(EmployeeModel employee) {
        this.employee = employee;       
    }

    @Parameterized.Parameters
    public static Collection data() {

        EmployeeModel employee = new EmployeeModel("1", "135791113", "Neider", "Moreno", "3124460030", "Activo");

        return Arrays.asList(new Object[]{
            employee
        });
    }

    @Test
    public void CreateEmployeeTest() {
        try {
            EmployeeDao empDao = new EmployeeDao();
            TestEmployeeDao empDaoTest = new  TestEmployeeDao();
            
            boolean resultado = empDao.Add(employee);
            assertTrue(resultado);  
            if (resultado) {
                empDaoTest.RemoveEmployeeTest(employee.getDocumentNumber());
            }
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
}
