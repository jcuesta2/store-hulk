/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.modelDao;

import com.cidenet.kardex.configuration.Conexion;
import com.cidenet.kardex.interfaces.General;
import com.cidenet.kardex.interfaces.Store;
import com.cidenet.kardex.model.OrderModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author jcuesta
 */
public class StoreDao implements General, Store {

    String SQL = "";
    private boolean statusMessage = false;
    Conexion conn = Conexion.getInstance();

    @Override
    public List List() {

        List<OrderModel> storeList = new ArrayList<>();

        try {
            SQL = "SELECT  t.id_productos_disponibles, tp.descripcion, sh.nombre,  talla.descripcion, "
                    + "t.stock, t.precio, t.precio_total "
                    + "FROM ProductosBodega b "
                    + "join TiposProductos tp on b.tipo_producto = tp.id_tipo_producto "
                    + "join Tallas talla on b.talla = talla.id_talla "
                    + "join SuperHeroes sh on b.superHeroe = sh.id_super_heroe "
                    + "join ProductosTienda t on t.id_productos_bodega = b.id_producto_bodega";

            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.resultado = conn.sentencia.executeQuery();

            while (conn.resultado.next()) {
                OrderModel store = new OrderModel();
                store.setStore(String.valueOf(conn.resultado.getInt(1)));
                store.setProductType(conn.resultado.getString(2));
                store.setPersonage(conn.resultado.getString(3));
                store.setSize(conn.resultado.getString(4));
                store.setQuantity(conn.resultado.getInt(5));
                store.setUnityValue(conn.resultado.getInt(6));
                store.setTotalValue(conn.resultado.getInt(7));
                storeList.add(store);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al listar productos de la tienda \n" + e);
        }
        return storeList;

    }

    @Override
    public List Search(String description) {
        List<OrderModel> storeList = new ArrayList<>();

        try {
            SQL = "SELECT  t.id_productos_disponibles, tp.descripcion, sh.nombre,  talla.descripcion, "
                    + "t.stock, t.precio, t.precio_total "
                    + "FROM ProductosBodega b "
                    + "join TiposProductos tp on b.tipo_producto = tp.id_tipo_producto "
                    + "join Tallas talla on b.talla = talla.id_talla "
                    + "join SuperHeroes sh on b.superHeroe = sh.id_super_heroe "
                    + "join ProductosTienda t on t.id_productos_bodega = b.id_producto_bodega "
                    + "WHERE tp.descripcion LIKE ?";

            conn.sentencia = conn.conexion.prepareStatement(SQL);
            String filter = "%".concat(description.toLowerCase()).concat("%");
            conn.sentencia.setString(1, filter);
            conn.resultado = conn.sentencia.executeQuery();

            while (conn.resultado.next()) {
                OrderModel store = new OrderModel();
                store.setStore(String.valueOf(conn.resultado.getInt(1)));
                store.setProductType(conn.resultado.getString(2));
                store.setPersonage(conn.resultado.getString(3));
                store.setSize(conn.resultado.getString(4));
                store.setQuantity(conn.resultado.getInt(5));
                store.setUnityValue(conn.resultado.getInt(6));
                store.setTotalValue(conn.resultado.getInt(7));
                storeList.add(store);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al listar productos de la tienda \n" + e);
        }
        return storeList;
    }

    @Override
    public List ListModal(int storeId) {
        List<OrderModel> storeList = new ArrayList<>();

        try {
            SQL = "SELECT  t.id_productos_disponibles, tp.descripcion, sh.nombre,  talla.descripcion, "
                    + "t.stock, t.precio "
                    + "FROM ProductosBodega b "
                    + "join TiposProductos tp on b.tipo_producto = tp.id_tipo_producto "
                    + "join Tallas talla on b.talla = talla.id_talla "
                    + "join SuperHeroes sh on b.superHeroe = sh.id_super_heroe "
                    + "join ProductosTienda t on t.id_productos_bodega = b.id_producto_bodega"
                    + "WHERE t.id_productos_disponibles = ?";

            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setInt(1, storeId);
            conn.resultado = conn.sentencia.executeQuery();

            while (conn.resultado.next()) {
                OrderModel store = new OrderModel();
                store.setOrderId(conn.resultado.getInt(1));
                store.setProductType(conn.resultado.getString(2));
                store.setPersonage(conn.resultado.getString(3));
                store.setSize(conn.resultado.getString(4));
                store.setQuantity(conn.resultado.getInt(5));
                store.setUnityValue(conn.resultado.getInt(6));
                storeList.add(store);
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al listar productos de la tienda \n" + e);
        }
        return storeList;
    }

    @Override
    public boolean Add(OrderModel store) {

        if (ValidateStoreExistence(store)) {
            if (!UpdateStock(store)) {
                statusMessage = false;
            }
        } else {
            try {
                SQL = "INSERT INTO ProductosTienda (id_productos_bodega, precio, stock, precio_total)"
                        + "values (?, ?, ?, ?)";
                conn.sentencia = conn.conexion.prepareStatement(SQL);
                conn.sentencia.setInt(1, store.getStorageId());
                conn.sentencia.setInt(2, store.getUnityValue());
                conn.sentencia.setInt(3, store.getQuantity());
                conn.sentencia.setInt(4, store.getQuantity() * store.getUnityValue());
                statusMessage = true;
                conn.sentencia.executeUpdate();
            } catch (SQLException e) {
                JOptionPane.showInputDialog("Error al agregar Producto a la tienda \n" + e);
            }
        }
        return statusMessage;
    }

    public boolean ValidateStoreExistence(OrderModel store) {
        try {
            SQL = "SELECT id_productos_disponibles, stock"
                    + " FROM ProductosTienda "
                    + " WHERE id_productos_bodega = ? ";

            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setInt(1, store.getStorageId());
            conn.resultado = conn.sentencia.executeQuery();

            if (conn.resultado.next()) {
                store.setStore(String.valueOf(conn.resultado.getInt(1)));
                store.setQuantity(conn.resultado.getInt(2) + store.getQuantity());
                store.setTotalValue((store.getQuantity() * store.getUnityValue()));
                statusMessage = true;
            }
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al listar productos de la tienda \n" + e);
        }
        return statusMessage;
    }

    public boolean UpdateStock(OrderModel store) {
        try {
            SQL = "UPDATE ProductosTienda SET precio = ?, stock = ?,  precio_total = ? "
                    + "WHERE id_productos_disponibles = ?";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setInt(1, store.getUnityValue());
            conn.sentencia.setInt(2, store.getQuantity());
            conn.sentencia.setInt(3, store.getTotalValue());
            conn.sentencia.setInt(4, Integer.parseInt(store.getStore()));
            conn.sentencia.executeUpdate();
            statusMessage = true;
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al actualizar stock en tienda \n" + e);
        }
        return statusMessage;

    }

}
