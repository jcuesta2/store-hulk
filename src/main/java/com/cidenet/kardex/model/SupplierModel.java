/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.model;

/**
 *
 * @author jcuesta
 */
public class SupplierModel {
    
    private int editorialId;
    private String description;
    private String status;

    @Override
    public String toString() {
        return "SupplierModel{" + "editorialId=" + editorialId + ", description=" + description + ", status=" + status + '}';
    }

    public int getEditorialId() {
        return editorialId;
    }

    public void setEditorialId(int editorialId) {
        this.editorialId = editorialId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public SupplierModel(int editorialId, String description, String status) {
        this.editorialId = editorialId;
        this.description = description;
        this.status = status;
    }

    public SupplierModel() {
    }
    
}
