<%-- 
    Document   : supplier
    Created on : Nov 19, 2019, 9:33:48 AM
    Author     : jcuesta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        <style>

            .table-fixed tbody {
                height: 300px;
                overflow-y: auto;
                width: 100%;
            }

            .table-fixed thead,
            .table-fixed tbody,
            .table-fixed tr,
            .table-fixed td,
            .table-fixed th {
                display: block;
            }

            .table-fixed tbody td,
            .table-fixed tbody th,
            .table-fixed thead > tr > th {
                float: left;
                position: relative;

                &::after {
                    content: '';
                    clear: both;
                    display: block;
                }
            }



            body {
                background: #74ebd5;
                background: -webkit-linear-gradient(to right, #74ebd5, #ACB6E5);
                background: linear-gradient(to right, #74ebd5, #ACB6E5);
                min-height: 100vh;

            }


        </style>

    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">
                <img src="/docs/4.3/assets/brand/bootstrap-solid.svg" width="30" height="30" alt="">
                Tienda-COMICS
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="../index.jsp">Inicio <span class="sr-only">(current)</span></a>
                    </li>                    
                    <li class="nav-item">
                        <a class="nav-link" href="StoreController?action=StoreList">Tienda</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="SalesController?action=SaleList">Ventas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="StorageController?action=StorageList">Bodega</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="OrderController?action=OrderList">Pedidos a proveedores</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Configuraciones
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="EmployeeController?action=EmployeeList">Empleados</a>                                                      
                            <a class="dropdown-item" href="ProductTypeController?action=ProductTypeList">Tipos de productos</a>
                            <a class="dropdown-item" href="SupplierController?action=SupplierList" title="Clic para listar">Proveedores</a>
                            <a class="dropdown-item" href="SizeController?action=SizeList">Tallas o tamaños</a>
                            <a class="dropdown-item" href="PriceOfProductController?action=PriceList">Cotización final de productos</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <div>
            <nav class="navbar navbar-dark" >
                <div class="row">
                    <div class="col-md-3 col-md-offset-9 text-right" >
                        <div class="btn-group" role="group">                            
                            <a class="btn btn-primary my-2 my-sm-0" href="PersonageController?action=ListActiveSupplier" title="Clic para crear">Nuevo</a>
                        </div>
                    </div>
                </div>
                <form action="PersonageController" method="GET" class="form-inline">
                    <label class=" my-2 my-sm-0" style="color:  red">*</label>&nbsp;<span class="glyphicon glyphicon-step-forward"></span>
                    <input name="txtsearchValue" class="form-control mr-sm-2" type="search" placeholder="Buscar personaje..."  maxlength="20"  title="Buscar personaje" required> 
                    <button name="action" class="btn btn-primary my-2 my-sm-0" type="submit" value="SearchPersonage"   title="Click para buscar">Buscar</button>&nbsp;<span class="glyphicon glyphicon-step-forward"></span>
                    <a class="btn btn-primary my-2 my-sm-0" role="button" href="PersonageController?action=PersonageList" title="Clic para listar">
                        Ver Todos
                    </a>&nbsp;<span class="glyphicon glyphicon-step-forward"></span>
                </form>

            </nav>
            <h4 align=center>Personajes</h4>
            <div class="container py-5" align=center>
                <div class="row">
                    <div class="col-md-12  mx-auto bg-white  rounded shadow"> <!-- Aqui esta el detalle-->                                        
                        <!-- Fixed header table-->
                        <div class="table-responsive">
                            <table class="table table-fixed">                            
                                <thead>                                    
                                <tr>     
                                    <th scope="col" class="col-3">Proveedor</th>
                                    <th scope="col" class="col-3">Personaje</th>
                                    <th scope="col" class="col-3">Estado</th>  
                                    <th scope="col" class="col-3">Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <c:if test="${personageList != null && not empty personageList}">
                                        <c:forEach var="personage" items="${personageList}">
                                            <tr>
                                                <td  width="10%" style="display: none;" scope="row">${personage.personageId}</td>   
                                                <td class="col-3">${personage.supplier}</td> 
                                                <td class="col-3">${personage.name}</td>
                                                <td class="col-3">${personage.status}</td>  
                                                <td class="col-3">        
                                                    <a id="btnEditarTar" class="btn btn-primary" role="button" href="PersonageController?action=EditModal&personageId=${personage.personageId}"   title="Click para Editar">Editar</a>                                    
                                                    <a class="btn btn-danger" role="button" href="PersonageController?action=StatusChange&personageId=${personage.personageId}"  title="Click para cambiar estado">Eliminar</a>                                
                                                </td> 
                                            </tr>
                                        </c:forEach>                                    
                                    </c:if>                        
                                </tbody>
                            </table>
                        </div>   
                    </div>
                </div>
            </div>
            <!-- Modal Crear Personage -->
            <c:if test="${openModal == 'Add' }">
                <div class="modal fade" id="ModalPersonage" tabindex="-1" role="dialog" aria-labelledby="ModalPersonage" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div id="divModalHeadCrear" class="modal-header">
                                <h5 class="modal-title" id="ModalPersonage">Crear Personaje</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form id="ModalPersonage" action="PersonageController" method="POST">
                                    <div class="form-group">
                                        <label style="color: red" for="message-text" class="col-form-label">* <label style="color: black" for="message-text" class="col-form-label">Proveedor : </label></label>
                                        <select class="form-control" name="txtsuppliers" placeholder="Selecione proveedor" required>
                                            <option selected>Selecciona el proveedor...</option>
                                            <c:forEach var="supplier" items="${listActiveSupplier}">                                                 
                                                <option value="${supplier.editorialId}">${supplier.description}</option>
                                            </c:forEach>                                                          
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label style="color: red" for="message-text" class="col-form-label">* <label style="color: black" for="message-text" class="col-form-label">Personaje : </label></label>
                                        <input type="text" class="form-control"  id="txtpersonage" name="txtpersonage" maxlength="20"  title="Nombre de personaje" required>      
                                    </div>
                                    <div class="form-group">
                                        <label style="color: red" for="message-text" class="col-form-label">* <label style="color: black" for="message-text" class="col-form-label">Estado : </label></label>
                                        <select class="custom-select" id="txtstatus" name="txtstatus" placeholder="Selecione(Activo o Inactivo)" required>
                                            <option selected>Estado...</option>
                                            <option value="activo">Activo</option>
                                            <option value="inactivo">Inactivo</option>                                            
                                        </select>
                                    </div>
                                    <div class="modal-footer">
                                        <button id="close" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                        <button id="btn-guardar" type="submit"  class="btn btn-info" name="action" value="PersonageAdd">Guardar</button>
                                    </div>
                                </form>   
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>
            <!-- Modal Editar Personage -->
            <c:if test="${openModal == 'Edit' }">
                <div class="modal fade" id="ModalPersonageEdit" tabindex="-1" role="dialog" aria-labelledby="ModalPersonageEdit" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div id="divModalHeadCrear" class="modal-header">
                                <h5 class="modal-title" id="ModalPersonage">Editar Personaje</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form id="ModalPersonageFormEdit" action="PersonageController" method="POST">                                                                               
                                    <div class="form-group">
                                        <label style="color: red" for="message-text" class="col-form-label">* <label style="color: black" for="message-text" class="col-form-label">Proveedor : </label></label>
                                        <select class="form-control" name="txtsuppliers"required> 
                                            <option selected>Selecciona el proveedor...</option>
                                            <c:forEach var="supplier" items="${listActiveSupplier}">                                                 
                                                <option value="${supplier.editorialId}">${supplier.description}</option>
                                            </c:forEach>                                                          
                                        </select>
                                    </div>

                                    <c:forEach var="personage" items="${EditModal}">
                                        <input id="txtpersonageId" name="txtpersonageId" type="hidden" value="${personage.personageId}">
                                        <div class="form-group">
                                            <label style="color: red" for="message-text" class="col-form-label">* <label style="color: black" for="message-text" class="col-form-label">Personaje : </label></label>
                                            <input type="text" class="form-control"  id="txtpersonage" name="txtpersonage" maxlength="20"  title="Nombre de personaje" value="${personage.name}" required>      
                                        </div>
                                        <div class="form-group">
                                            <label style="color: red" for="message-text" class="col-form-label">* <label style="color: black" for="message-text" class="col-form-label">Estado : </label></label>
                                            <select class="custom-select" id="txtstatus" name="txtstatus" placeholder="Selecione(Activo o Inactivo)" required>
                                                <option selected>Seleccione el estado...</option>
                                                <option value="activo">Activo</option>
                                                <option value="inactivo">Inactivo</option>                                            
                                            </select>
                                        </div>
                                        <div class="modal-footer">
                                            <button id="close" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                            <button id="btn-guardar" type="submit"  class="btn btn-info" name="action" value="PersonageEdit">Guardar</button>
                                        </div>
                                    </c:forEach>
                                </form>   
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>
            <!-- Modal Response -->
            <c:if test="${not empty codeStatus }">
                <div class="container">
                    <div class="modal fade" id="ModalResponse" role="dialog">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content"> 
                                <div id="divModalHeadConf"  class="modal-header">
                                    <h4 class="modal-title">${codeStatus}</h4>                        
                                </div>
                                <div style="alignment-baseline: central" class="modal-body">
                                    <p>${message}</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>
            <script>
                $("#ModalResponse").modal("show");
                $("#ModalPersonage").modal("show");
                $("#ModalPersonageEdit").modal("show");

            </script>

    </body>
</html>
