/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.interfaces;

import com.cidenet.kardex.model.PriceOfProductModel;
import java.util.List;

/**
 *
 * @author jcuesta
 */
public interface PriceOfProductType {

    public boolean Add(PriceOfProductModel priceProduct);

    public List ValidatePriceExistence(PriceOfProductModel priceProduct);

    public boolean ChangeStatus(PriceOfProductModel priceProduct);

    public boolean Edit(PriceOfProductModel priceProduct);
}
