/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.model;

/**
 *
 * @author jcuesta
 */
public class ProductTypeModel {
    private int productTypeId;
    private String description;
    private String status;

   

    @Override
    public String toString() {
        return "ProductTypeModel{" + "productTypeId=" + productTypeId + ", description=" + description +  ", status=" + status +'}';
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(int productTypeId) {
        this.productTypeId = productTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ProductTypeModel(int productTypeId, String description, String status) {
        this.productTypeId = productTypeId;
        this.description = description;
        this.status = status;
    }

    public ProductTypeModel() {
    }
}
