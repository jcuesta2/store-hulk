/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author jcuesta
 */
public class Conexion {

    private static Conexion instance;

    public ResultSet resultado;
    public PreparedStatement sentencia;
    public Connection conexion;

    static {
        instance = new Conexion();
        instance.Connect();
    }

    private Conexion() {
    }

    public void Connect() {
        String url = "jdbc:postgresql://localhost:5432/KARDEX";
        String user = "jhonatan";
        String password = "postgres";

        // Importar driver de conexion
        try {
            Class.forName("org.postgresql.Driver");
            conexion = DriverManager.getConnection(url, user, password);

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "conexion erronea" + " " + error);
        }

    }

    public void Disconnect() {
        try {
            conexion.close();

        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "desconexion erronea" + " " + error);
        }
    }

    public static Conexion getInstance() {
        return instance;
    }
}
