/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.controller;

import com.cidenet.kardex.model.EmployeeModel;
import com.cidenet.kardex.model.OrderModel;
import com.cidenet.kardex.model.PersonageModel;
import com.cidenet.kardex.model.ProductTypeModel;
import com.cidenet.kardex.model.SizeModel;
import com.cidenet.kardex.modelDao.EmployeeDao;
import com.cidenet.kardex.modelDao.PersonageDao;
import com.cidenet.kardex.modelDao.ProductTypeDao;
import com.cidenet.kardex.modelDao.SalesDao;
import com.cidenet.kardex.modelDao.SizeDao;
import com.cidenet.kardex.modelDao.StorageDao;
import com.cidenet.kardex.modelDao.StoreDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jcuesta
 */
@WebServlet(name = "StoreController", urlPatterns = {"/StoreController"})
public class StoreController extends HttpServlet {

    List<OrderModel> storeList, storeModal = new ArrayList<>();
    List<ProductTypeModel> proAssetList = new ArrayList<>();
    List<SizeModel> sizeAssetList = new ArrayList<>();
    List<PersonageModel> personageAssetList = new ArrayList<>();
    List<EmployeeModel> employeeAssetList = new ArrayList<>();

    ProductTypeDao product = new ProductTypeDao();
    SizeDao size = new SizeDao();
    PersonageDao personage = new PersonageDao();
    StorageDao storage = new StorageDao();
    StoreDao store = new StoreDao();
    EmployeeDao employee = new EmployeeDao();
    SalesDao sale = new SalesDao();

    OrderModel storeModel = new OrderModel();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet StoreController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet StoreController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String openModal = "";
        String description = "";
        String action = request.getParameter("action");
        switch (action) {
            case "StoreList": {

            }
            break;
            case "StoreSearch": {
                description = request.getParameter("txtproduct");

            }
            break;

            case "JoinToSelect": {
                openModal = "Solicitar";
                proAssetList = product.AssetList();
                sizeAssetList = size.AssetList();
                personageAssetList = personage.AssetList();

                request.setAttribute("proAssetList", proAssetList);
                request.setAttribute("sizeAssetList", sizeAssetList);
                request.setAttribute("personageAssetList", personageAssetList);

            }
            break;
            case "ListModal": {
                String storeId = request.getParameter("storeId");
                for (OrderModel storeListItem : storeList) {
                    storeModal.clear();
                    if (storeListItem.getStore().equals(storeId)) {
                        openModal = "Vender";

                        storeModal.add(storeListItem);
                        employeeAssetList = employee.AssetList();

                        request.setAttribute("storeModal", storeModal);
                        request.setAttribute("employeeAssetList", employeeAssetList);

                        break;

                    }
                }

            }
            break;
        }
        if (action.equals("StoreSearch")) {
            storeList = store.Search(description);
        } else {
            storeList = store.List();

        }
        request.setAttribute("openModal", openModal);
        request.setAttribute("storeList", storeList);
        request.getRequestDispatcher("com.cidenet.kardex.view/store.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String codeStatus = "";
        String message = "";
        String action = request.getParameter("action");
        switch (action) {
            case "StoreAdd": {
                storeModel.setProductType(request.getParameter("txtproduct"));
                storeModel.setPersonage(request.getParameter("txtpersonage"));
                storeModel.setSize(request.getParameter("txtsize"));
                storeModel.setQuantity(Integer.parseInt(request.getParameter("txtquantity")));
                storeList = storage.UpdateStorageStock(storeModel);
                if (storeList != null) {
                    storeModel.setStorageId(storeList.get(0).getStorageId());
                    storeModel.setUnityValue(storeList.get(0).getUnityValue());
                    if (store.Add(storeModel)) {
                        codeStatus = "Success";
                        message = "Producto agregado a la tienda";
                    } else {
                        codeStatus = "Error";
                        message = "Error agregado producto a la tienda";
                    }
                } else {
                    codeStatus = "Alerta";
                    message = "Producto no disponible en bodega";
                }
            }
            break;
            case "Sale": {
                int quantityStore = Integer.parseInt(request.getParameter("txtstoreQuantity"));
                int quantitySale = Integer.parseInt(request.getParameter("txtquantitysale"));

                if (quantityStore >= quantitySale) {
                    
                    int unityValue = Integer.parseInt(request.getParameter("txtunityValue"));
                    String desArticle = request.getParameter("txtdesArticle");
                    String size = request.getParameter("txtsize");
                    
                    
                    storeModel.setStore(request.getParameter("txtstoreid"));
                    storeModel.setQuantity(quantityStore - quantitySale);
                    storeModel.setUnityValue(unityValue);
                    storeModel.setTotalValue(unityValue * storeModel.getQuantity());
                    storeModel.setProductType(desArticle);
                    storeModel.setSize(size);                    
                    if (store.UpdateStock(storeModel)) {
                        
                        storeModel.setQuantity(quantitySale);
                        sale.Add( storeModel);
                        codeStatus = "Success";
                        message = "Producto vendido";
                    }
                } else {
                    codeStatus = "Error";
                    message = "La tienda no cuenta con la cantidad a vender";
                }

            }
        }
        storeList = store.List();
        request.setAttribute("codeStatus", codeStatus);
        request.setAttribute("message", message);
        request.setAttribute("storeList", storeList);
        request.getRequestDispatcher("com.cidenet.kardex.view/store.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
