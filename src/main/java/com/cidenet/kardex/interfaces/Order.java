/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.interfaces;

import com.cidenet.kardex.model.OrderModel;
import java.util.List;

/**
 *
 * @author jcuesta
 */
public interface Order {

    public boolean Add(OrderModel order);

    public boolean ValidateOrderExistence(OrderModel order);

    public List OrderReceive(int id);

    public List OrderFilter(String dateStart, String dateLimit);

    public boolean ChangeStatus(int orderId, String value);
    
    public boolean Cancel(int orderId);
    
    
}
