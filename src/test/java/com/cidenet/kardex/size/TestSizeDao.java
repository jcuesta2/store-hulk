/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cidenet.kardex.size;

import com.cidenet.kardex.configuration.Conexion;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author jcuesta
 */
public class TestSizeDao {
    String SQL = "";
    Conexion conn = Conexion.getInstance();
    
    public void RemoveSizeTest(String description) {
        try {
            SQL = "DELETE FROM tallas WHERE descripcion = ?";
            conn.sentencia = conn.conexion.prepareStatement(SQL);
            conn.sentencia.setString(1, description);
            conn.sentencia.executeUpdate();
        } catch (SQLException e) {
            JOptionPane.showInputDialog("Error al eliminar talla creado en la prueba \n" + e);
        }
    }
}
